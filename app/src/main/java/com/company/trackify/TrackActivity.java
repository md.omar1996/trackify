package com.company.trackify;
import android.Manifest;
import android.app.AlarmManager;
import android.app.AlertDialog;
import android.app.PendingIntent;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.DrawableRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.animation.GlideAnimation;
import com.bumptech.glide.request.target.SimpleTarget;
import com.company.trackify.Fragments.TeamFragment;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.GoogleApiClient.ConnectionCallbacks;
import com.google.android.gms.common.api.GoogleApiClient.OnConnectionFailedListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.GoogleMap.OnMyLocationButtonClickListener;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import android.support.v4.app.FragmentActivity;


import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Text;

import java.io.IOException;
import java.lang.reflect.Member;
import java.text.DateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import static java.security.AccessController.getContext;

/**
 * Created by mayank on 5/10/16.
 */
public class TrackActivity extends AppCompatActivity implements OnMapReadyCallback, OnMyLocationButtonClickListener, ConnectionCallbacks, OnConnectionFailedListener {

    protected static final String TAG = "location-updates-sample";
    public static final String GET_LOCATION  = "https://agro-mycollege.c9users.io/api/location/get";
    public int LOCATION_INTERVAL = 10 * 1000;
    public int LOCATION_DISTANCE = 10;
    public static int time = 5;
    public static int distance = 100;
    JSONObject jsonObject;
    SharedPreferences shared;
    GoogleMap mMap;
    private boolean mPermissionDenied = false;
    LatLng latlng;
    private BroadcastReceiver NetworkChangeListener;
    public static int isConnected = 1;
    String dateTime = "";
    Bitmap image;

    private View mCustomMarkerView;
    private ImageView mMarkerImageView;

    Marker mCurrLocationMarker;


    /**
     * The desired interval for location updates. Inexact. Updates may be more or less frequent.
     */
    public static final long UPDATE_INTERVAL_IN_MILLISECONDS = 10000;

    /**
     * The fastest rate for active location updates. Exact. Updates will never be more frequent
     * than this value.
     */
    public static final long FASTEST_UPDATE_INTERVAL_IN_MILLISECONDS =
            UPDATE_INTERVAL_IN_MILLISECONDS / 2;

    // Keys for storing activity state in the Bundle.
    protected final static String REQUESTING_LOCATION_UPDATES_KEY = "requesting-location-updates-key";
    protected final static String LOCATION_KEY = "location-key";
    protected final static String LAST_UPDATED_TIME_STRING_KEY = "last-updated-time-string-key";

    protected GoogleApiClient mGoogleApiClient;

    protected Location mCurrentLocation;

    protected String mLastUpdateTime;
    /**
     * Stores parameters for requests to the FusedLocationProviderApi.
     */
    protected LocationRequest mLocationRequest;

    private static final int LOCATION_PERMISSION_REQUEST_CODE = 1;

    TextView textview;
    private String userid = "";
    private String memberid = "";
    private String deviceid = "";
    private String token = "";
    static LinearLayout ly=null;
    final String PREF_NAME = "USER";
    Handler handler = null;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_track);

        ly = (LinearLayout) findViewById(R.id.connectionError);
        System.out.println(ly.getId());

        mCustomMarkerView = ((LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.view_custom_marker, null);
        mMarkerImageView = (ImageView) mCustomMarkerView.findViewById(R.id.profile_image);

        NetworkChangeListener = new NetworkChangeListener();
        IntentFilter intentFilter = new IntentFilter(
                "android.net.conn.CONNECTIVITY_CHANGE");
        this.registerReceiver(NetworkChangeListener, intentFilter);
//        if(extras.getString("DEVICEID").isEmpty()){
//            Toast.makeText(TrackActivity.this, "Not Authorized to get Tracked", Toast.LENGTH_SHORT).show();
//            finish();
//        }else{
//            deviceid = extras.getString("DEVICEID");
//        }
//        SharedPreferences shared = getSharedPreferences(PREF_NAME, MODE_PRIVATE);
//        userid = (shared.getString("userid","0"));
//        token = (shared.getString("token",null));

        textview = (TextView) findViewById(R.id.location_text_view);
        textview.setText("Getting Location...");
        SupportMapFragment mapFragment =
                (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
        mapFragment.setRetainInstance(true);
        buildGoogleApiClient();

    }

    /**
     * Builds a GoogleApiClient. Uses the {@code #addApi} method to request the
     * LocationServices API.
     */
    protected synchronized void buildGoogleApiClient() {
        Log.i(TAG, "Building GoogleApiClient");
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();

        createLocationRequest();
    }

    protected void startLocationUpdates() {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            enableMyLocation();
            return;
        }
    }

    protected void createLocationRequest() {
        mLocationRequest = new LocationRequest();

        // Sets the desired interval for active location updates. This interval is
        // inexact. You may not receive updates at all if no location sources are available, or
        // you may receive them slower than requested. You may also receive updates faster than
        // requested if other applications are requesting location at a faster interval.
        mLocationRequest.setInterval(UPDATE_INTERVAL_IN_MILLISECONDS);

        // Sets the fastest rate for active location updates. This interval is exact, and your
        // application will never receive updates faster than this value.
        mLocationRequest.setFastestInterval(FASTEST_UPDATE_INTERVAL_IN_MILLISECONDS);

        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
    }
    public void setNetStatus(int status){


        if(ly != null){
            if(status == 0){
                ly.setVisibility(View.VISIBLE);
                isConnected = 0;
            }else{
                ly.setVisibility(View.GONE);
                isConnected = 1;
            }
        }else{
            Log.d("oihas",String.valueOf(status));

        }
        //Toast.makeText(getApplicationContext()  , String.valueOf(status), Toast.LENGTH_SHORT).show();
    }
    @Override
    protected void onStart() {
        super.onStart();
        mGoogleApiClient.connect();

    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    protected void onDestroy() {
        Log.e("track activity","in onDestroy");
        handler.removeCallbacksAndMessages(null);
        mGoogleApiClient.disconnect();
        super.onDestroy();
        mCurrLocationMarker.remove();
        this.unregisterReceiver(this.NetworkChangeListener);
        //Toast.makeText(DriverActivity.this, "Destroy", Toast.LENGTH_SHORT).show();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        mMap.setOnMyLocationButtonClickListener(this);
        mMap.getUiSettings().setRotateGesturesEnabled(false);
        enableMyLocation();
        Log.e("track act","in on map ready");
        mMap.setInfoWindowAdapter(new MarkerInfoWindowAdapter());
        trackresource();

    }

    public void trackresource() {
        Log.e("track act", "in trackresource");

        handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                shared = getSharedPreferences(PREF_NAME, MODE_PRIVATE);
                userid = (shared.getString("userid","0"));
                token = (shared.getString("token",null));

                memberid = getIntent().getExtras().getString("MEMBERID");
                Log.e("track act", userid + "  " +token+ "  " +memberid);
                shared = getSharedPreferences(PREF_NAME, MODE_PRIVATE);
                time = shared.getInt("time",0);
                Log.e("track",Integer.toString(time)) ;
                distance = shared.getInt("distance", 0);
                LOCATION_INTERVAL = time * 1000;
                LOCATION_DISTANCE = distance;

                StringRequest stringRequest = new StringRequest(Request.Method.POST, GET_LOCATION, new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.e("track act", "in on response");
                        if (mCurrLocationMarker!=null) {
                            mCurrLocationMarker.remove();
                        }
                        try {
                            Log.e("RESP",response);
                            jsonObject=new JSONObject(response);
                            int resp=jsonObject.getInt("resp");
                            String msg=jsonObject.getString("msg");
                            if(msg.equals("Last Location not known")){
                                Toast.makeText(TrackActivity.this, "Last location not known", Toast.LENGTH_SHORT).show();
                                return;
                            }
                            JSONObject jo=new JSONObject(msg);
                            Double latitude=jo.getDouble("lat");
                            Double longitude=jo.getDouble("lng");
                            dateTime = jo.getString("created_dt");
                            if(resp ==  1){
                                latlng=new LatLng(latitude,longitude);


                                Geocoder geocoder;
                                List<Address> addresses;
                                geocoder = new Geocoder(getApplicationContext(), Locale.getDefault());

                                addresses = geocoder.getFromLocation(latitude, longitude, 2); // Here 1 represent max location result to returned, by documents it recommended 1 to 5

                                String address = addresses.get(0).getAddressLine(0);
                                if (address.equals("")){
                                    String city = addresses.get(0).getLocality();
                                    textview.setText(city);
                                }
                                String city = addresses.get(0).getLocality();

                                textview.setText(address + ", " +city);


                                if (image == null) {
                                    mMap.moveCamera(CameraUpdateFactory.newLatLng(latlng));
                                    mMap.animateCamera(CameraUpdateFactory.zoomTo(15));
                                    mMap.setInfoWindowAdapter(new MarkerInfoWindowAdapter());
                                    Log.e("jhell","hello");
                                    MarkerOptions markerOptions = new MarkerOptions();
                                    markerOptions.position(new LatLng(latitude, longitude));
                                    markerOptions.icon(BitmapDescriptorFactory.fromBitmap(getMarkerBitmapFromView(mCustomMarkerView, R.drawable.userimage)));
                                    mCurrLocationMarker = mMap.addMarker(markerOptions);
                                    mCurrLocationMarker.showInfoWindow();
                                    Glide.with(getApplicationContext()).
                                            load("https://agro-mycollege.c9users.io/profileImage/" + memberid + ".png")
                                            .asBitmap()
                                            .fitCenter()
                                            .error(R.drawable.userimage)
                                            .into(new SimpleTarget<Bitmap>() {
                                                @Override
                                                public void onResourceReady(Bitmap bitmap, GlideAnimation<? super Bitmap> glideAnimation) {
                                                    Log.e("track", "we are here now!" + memberid);
                                                    image = bitmap;
                                                    mCurrLocationMarker.remove();
                                                    MarkerOptions markerOptions = new MarkerOptions();
                                                    markerOptions.position(latlng);
                                                    markerOptions.icon(BitmapDescriptorFactory.fromBitmap(getMarkerBitmapFromView(mCustomMarkerView, image)));
                                                    mCurrLocationMarker = mMap.addMarker(markerOptions);
                                                    mCurrLocationMarker.showInfoWindow();
                                                }
                                            });

                                }else{
                                    Log.e("trackifuy","in else");
                                    MarkerOptions markerOptions = new MarkerOptions();
                                    markerOptions.position(latlng);
                                    markerOptions.icon(BitmapDescriptorFactory.fromBitmap(getMarkerBitmapFromView(mCustomMarkerView, image)));
                                    mCurrLocationMarker = mMap.addMarker(markerOptions);
                                    mCurrLocationMarker.showInfoWindow();

                                }
                            }else{
                                Toast.makeText(TrackActivity.this, msg, Toast.LENGTH_SHORT).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                },
                        new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {

                                if (error instanceof TimeoutError || error instanceof NoConnectionError){
                                    Toast.makeText(TrackActivity.this, "Please check your internet connection!", Toast.LENGTH_SHORT).show();
                                }
                            }
                        }
                ) {
                    protected Map<String, String> getParams() {
                        Map<String, String> params = new HashMap<>();
                        params.put("user_id", userid);
                        params.put("token", token);
                        params.put("member_id",memberid);

                        return params;
                    }
                };
                RequestQueue requestQueue = Volley.newRequestQueue(TrackActivity.this);
                requestQueue.add(stringRequest);
                handler.postDelayed(this, LOCATION_INTERVAL);
            }
        },LOCATION_INTERVAL);

//        handler.postDelayed(new Runnable() {
//            @Override
//            public void run() {
//                //Do something after 20 seconds
//                String uri = Uri.parse("http://www.agro-mycollege.c9users.io/api/locations/get")
//                        .buildUpon()
//                        .appendQueryParameter("deviceid", deviceid)
//                        .appendQueryParameter("userid", String.valueOf(userid))
//                        .appendQueryParameter("token", token)
//                        .build().toString();
//                Log.d(TAG, uri);
//                RequestQueue requestQueue;
//
//                requestQueue = Volley.newRequestQueue(getApplicationContext());
//                JsonObjectRequest jor = new JsonObjectRequest(Request.Method.GET, uri, null,
//                        new Response.Listener<JSONObject>() {
//                            @Override
//                            public void onResponse(JSONObject response) {
//
//                                try{
//
//                                    int resp = response.getInt("resp");
//                                    if(resp == 1){
//                                        JSONObject msg = response.getJSONObject("msg");
//                                        textview.setText(msg.getString("address"));
//                                        m.setPosition(new LatLng(msg.getDouble("lat"),msg.getDouble("lng")));
//                                        m.setRotation(msg.getLong("bearing"));
//
//                                    }else{
//                                        Toast.makeText(TrackActivity.this,response.getString("msg") , Toast.LENGTH_SHORT).show();
//                                    }
//
//                                }catch(JSONException e)
//                                {
//                                    e.printStackTrace();
//                                }
//                            }
//                        },
//                        new Response.ErrorListener() {
//                            @Override
//                            public void onErrorResponse(VolleyError error) {
//
//                                if (error instanceof TimeoutError || error instanceof NoConnectionError) {
//                                    //Log.e("Volley",error.getMessage());
//                                    runOnUiThread(new Runnable() {
//                                        @Override
//                                        public void run() {
//                                            Toast.makeText(TrackActivity.this, "Please check internet connection", Toast.LENGTH_SHORT).show();
//                                        }
//                                    });
//                                }else{
//                                    Toast.makeText(TrackActivity.this, "Tracking Server not responding", Toast.LENGTH_SHORT).show();
//                                }
//                            }
//                        }
//                );
//                requestQueue.add(jor);
//                handler.postDelayed(this, 10000);
//            }
//        }, 1000);
    }
    private Bitmap getMarkerBitmapFromView(View view, @DrawableRes int resId) {

        mMarkerImageView.setImageResource(resId);
        view.measure(View.MeasureSpec.UNSPECIFIED, View.MeasureSpec.UNSPECIFIED);
        view.layout(0, 0, view.getMeasuredWidth(), view.getMeasuredHeight());
        view.buildDrawingCache();
        Bitmap returnedBitmap = Bitmap.createBitmap(view.getMeasuredWidth(), view.getMeasuredHeight(),
                Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(returnedBitmap);
        canvas.drawColor(Color.WHITE, PorterDuff.Mode.SRC_IN);
        Drawable drawable = view.getBackground();
        if (drawable != null)
            drawable.draw(canvas);
        view.draw(canvas);
        return returnedBitmap;
    }
    public class MarkerInfoWindowAdapter implements GoogleMap.InfoWindowAdapter
    {
        public MarkerInfoWindowAdapter()
        {
        }

        @Override
        public View getInfoWindow(Marker marker)
        {
            return null;
        }

        @Override
        public View getInfoContents(Marker marker)
        {
            View v  = getLayoutInflater().inflate(R.layout.infowindow_layout, null);

//            ImageView markerIcon = (ImageView) v.findViewById(R.id.marker_icon);

            TextView markerLabel = (TextView)v.findViewById(R.id.marker_label);
            String username = getIntent().getExtras().getString("MEMBER_NAME");
//            markerIcon.setImageBitmap(BitmapFactory.decodeFile("/storage/emulated/0/Trackify/ProfilePhoto.jpg"));
            markerLabel.setText(username + " | " + dateTime);

            return v;
        }
    }
    private Bitmap getMarkerBitmapFromView(View view, Bitmap bitmap) {

        mMarkerImageView.setImageBitmap(bitmap);
        view.measure(View.MeasureSpec.UNSPECIFIED, View.MeasureSpec.UNSPECIFIED);
        view.layout(0, 0, view.getMeasuredWidth(), view.getMeasuredHeight());
        view.buildDrawingCache();
        Bitmap returnedBitmap = Bitmap.createBitmap(view.getMeasuredWidth(), view.getMeasuredHeight(),
                Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(returnedBitmap);
        canvas.drawColor(Color.WHITE, PorterDuff.Mode.SRC_IN);
        Drawable drawable = view.getBackground();
        if (drawable != null)
            drawable.draw(canvas);
        view.draw(canvas);
        return returnedBitmap;
    }

    private void enableMyLocation() {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {
            // Permission to access the location is missing.
            PermissionUtils.requestPermission(this, LOCATION_PERMISSION_REQUEST_CODE,
                    Manifest.permission.ACCESS_FINE_LOCATION, true);
        } else if (mMap != null) {
            // Access to the location has been granted to the app.
            mMap.setMyLocationEnabled(true);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        if (requestCode != LOCATION_PERMISSION_REQUEST_CODE) {
            return;
        }

        if (PermissionUtils.isPermissionGranted(permissions, grantResults,
                Manifest.permission.ACCESS_FINE_LOCATION)) {
            // Enable the my location layer if the permission has been granted.
            enableMyLocation();
        } else {
            // Display the missing permission error dialog when the fragments resume.
            mPermissionDenied = true;
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
//        MenuInflater inflater = getMenuInflater();
//        inflater.inflate(R.menu.menu_driver, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
//        switch (item.getItemId()) {
//
//            // action with ID action_refresh was selected
//            case R.id.action_emergency:
//                Toast.makeText(this, "Asking authorized member to contact you", Toast.LENGTH_LONG)
//                        .show();
//                makeemergencycall();
//                break;
//            // action with ID action_settings was selected
//            case R.id.action_support:
//                Toast.makeText(this, "We will call you in 10 minutes", Toast.LENGTH_LONG)
//                        .show();
//                break;
//            case android.R.id.home:
//                finish();
//                break;
//            default:
//                break;
//        }

        return true;
    }


    private void exitapp(){
        runOnUiThread(new Runnable() {
            public void run() {
                new AlertDialog.Builder(TrackActivity.this)
                        .setMessage("Are you sure you want to end the tracking?")
                        .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                mGoogleApiClient.disconnect();
                                finish();
                            }
                        })
                        .setNegativeButton(android.R.string.cancel,null )
                        .create()
                        .show();
            }
        });
    }

    @Override
    protected void onResumeFragments() {
        super.onResumeFragments();
        if (mPermissionDenied) {
            // Permission was not granted, display error dialog.
            showMissingPermissionError();
            mPermissionDenied = false;
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        // Within {@code onPause()}, we pause location updates, but leave the
        // connection to GoogleApiClient intact.  Here, we resume receiving
        // location updates if the user has requested them.

        if (mGoogleApiClient.isConnected()) {
            startLocationUpdates();
        }
    }


    /**
     * Displays a dialog with error message explaining that the location permission is missing.
     */
    private void showMissingPermissionError() {
        PermissionUtils.PermissionDeniedDialog
                .newInstance(true).show(getSupportFragmentManager(), "dialog");
    }

    @Override
    public boolean onMyLocationButtonClick() {
        //Toast.makeText(this, "Trying to get my location", Toast.LENGTH_SHORT).show();
        Log.d("Latitude", "Location listener called");
        if(latlng != null){
            mMap.moveCamera(CameraUpdateFactory.newLatLng(latlng));
            mMap.animateCamera(CameraUpdateFactory.zoomTo(8));
        }else {
            Toast.makeText(TrackActivity.this, "Trying to get location", Toast.LENGTH_SHORT).show();
        }

        return false;
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        Log.d(TAG,"connected");
        if (mCurrentLocation == null) {
            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                enableMyLocation();
            }else{
                mCurrentLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
                if(mCurrentLocation != null){
                    latlng = new LatLng(mCurrentLocation.getLatitude(),mCurrentLocation.getLongitude());
                }
                if (mCurrentLocation != null) {
                    mLastUpdateTime = DateFormat.getTimeInstance().format(new Date());
                }
                startLocationUpdates();
            }
        }
    }

    @Override
    public void onConnectionSuspended(int i) {
        mGoogleApiClient.connect();
        Log.d(TAG,"connection suspended");
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        Log.d(TAG,"connection failed");
    }

    @Override
    public void onBackPressed() {
        handler.removeCallbacksAndMessages(null);
        exitapp();
    }



}
