package com.company.trackify;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.TextInputEditText;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.company.trackify.CustomAdapters.CustomListAdapterForTeam;
import com.company.trackify.helper.Constants;
import com.company.trackify.helper.DatabaseHelper;
import com.company.trackify.helper.MemberEntries;
import com.company.trackify.helper.MySingleton;
import com.company.trackify.helper.TeamEntries;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

/**
 * Created by subodh on 12/3/17.
 */

public class Teams extends AppCompatActivity implements View.OnClickListener{
    Context context;
    JSONObject jsonObject;
    CustomListAdapterForTeam adapter;
    ListView listView;
    DatabaseHelper db;
    FloatingActionButton fab;
    String token="";
    String userid="";
    SharedPreferences shared;
    final String PREF_NAME = "USER";
    RelativeLayout layout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_teams);
        layout = (RelativeLayout)findViewById(R.id.first_team);
        layout.setVisibility(View.GONE);
        shared = getSharedPreferences(PREF_NAME, MODE_PRIVATE);
        userid = shared.getString("userid","0");
        token = shared.getString("token","0");
        db = new DatabaseHelper(getApplicationContext());
        final List<TeamEntries> teamEntries = db.getTeamEntries();
        listView=(ListView)findViewById(R.id.teams_list);
        adapter = new CustomListAdapterForTeam(Teams.this, teamEntries);
        listView.setAdapter(adapter);
        if(adapter.isEmpty()){
            Log.e("teams","empty");
            layout.setVisibility(View.VISIBLE);
            Button button = (Button)layout.findViewById(R.id.create_team);
            button.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    addTeam();
                }
            });
        }

        fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(this);
        showTeams();
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                try {

                    TeamEntries teamEntries1= (TeamEntries) adapterView.getItemAtPosition(i);
                    String id =teamEntries1.getId();
                    Log.e("user id",id);
                    Intent intent = new Intent(getApplicationContext(),Members.class);
                    intent.putExtra("DEVICEID",id);
                    startActivity(intent);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }


        @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_main, menu);
        return true;
    }
    public void showTeams(){
        final List<TeamEntries> teamEntries = db.getTeamEntries();
        listView=(ListView)findViewById(R.id.teams_list);
        adapter = new CustomListAdapterForTeam(Teams.this, teamEntries);
        listView.setAdapter(adapter);
    }
    public void refresh(){

//        final ProgressDialog pd = new ProgressDialog(Teams.this);
//        pd.setCancelable(false);
//        pd.setCanceledOnTouchOutside(false);
//        pd.setMessage("Refreshing.....");
//        pd.show();
        StringRequest stringRequest = new StringRequest(Request.Method.POST, Constants.sync, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
//                pd.dismiss();
                try {
                    jsonObject=new JSONObject(response);
                    int resp=jsonObject.getInt("resp");
                    Log.d("teams",response);
                    if (resp== 0){
                        String msg=jsonObject.getString("msg");
                        Toast.makeText(Teams.this, msg, Toast.LENGTH_SHORT).show();
                    }else{
                        DatabaseHelper db;
                        db = new DatabaseHelper(getApplicationContext());
                        JSONArray teamArray = jsonObject.getJSONArray("team");
                        db.truncateTeam();
                        for(int i = 0; i < teamArray.length() ; i++ ){
                            String id = teamArray.getJSONObject(i).getString("id");
                            String user_id = teamArray.getJSONObject(i).getString("user_id");
                            String team_name = teamArray.getJSONObject(i).getString("team_name");
                            String team_desc = teamArray.getJSONObject(i).getString("team_desc");
                            String createdAt = teamArray.getJSONObject(i).getString("created_dt");
                            String is_active = teamArray.getJSONObject(i).getString("is_active");
                            TeamEntries userEntriesHelper=new TeamEntries(id, user_id, team_name, team_desc,createdAt,is_active);
                            Long ID = db.createTeamEntry(userEntriesHelper);
                            Log.d("LOG",ID.toString()+"hello world");
                        }
                        db.truncateMember();
                        JSONArray memberArray = jsonObject.getJSONArray("member");
                        for(int i = 0; i < memberArray.length() ; i++ ){
                            String id = memberArray.getJSONObject(i).getString("id");
                            String team_id = memberArray.getJSONObject(i).getString("team_id");
                            String member_id = memberArray.getJSONObject(i).getString("member_id");
                            String name = memberArray.getJSONObject(i).getString("name");
                            String createdAT = memberArray.getJSONObject(i).getString("created_dt");
                            String is_active = memberArray.getJSONObject(i).getString("is_active");
                            MemberEntries memberEntries = new MemberEntries(id, team_id, member_id, name, createdAT, is_active);
                            Long ID=db.createMemberEntry(memberEntries);
                            Log.d("LOG",ID.toString()+"hello omar");
                            Log.d("LOG",name+"hello omar");

                        }

                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                final List<TeamEntries> teamEntries = db.getTeamEntries();
                listView=(ListView)findViewById(R.id.teams_list);
                adapter = new CustomListAdapterForTeam(Teams.this, teamEntries);
                listView.setAdapter(adapter);
            }
        },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
//                        pd.dismiss();
                        if (error instanceof TimeoutError || error instanceof NoConnectionError){
                            Toast.makeText(Teams.this, "Please check your internet connection!", Toast.LENGTH_SHORT).show();
                        }
                    }
                }
        ) {
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("user_id", userid);
                params.put("token", token);
                return params;
            }
        };
        MySingleton singleton=MySingleton.getInstance(this);
        singleton.getRequestQueue().add(stringRequest);
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {

            // action with ID action_settings was selected
            case R.id.action_refresh:
                final ProgressDialog pd = new ProgressDialog(Teams.this);
                pd.setCancelable(false);
                pd.setCanceledOnTouchOutside(false);
                pd.setMessage("Refreshing.....");
                pd.show();
                refresh();
                pd.dismiss();
                break;

            default:
        }

        return true;
    }
    public void addTeam(){
        final Dialog dialog =new Dialog(Teams.this);
        dialog.setContentView(R.layout.dialog_add_team);
        dialog.setTitle("Add Team");
        dialog.show();
        Button button_add=(Button)dialog.findViewById(R.id.btn_add);

        button_add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                TextInputEditText nameOfTeam= (TextInputEditText)dialog.findViewById(R.id.name);
                TextInputEditText descOfTeam= (TextInputEditText)dialog.findViewById(R.id.desc);
                final String name=nameOfTeam.getText().toString();
                final String desc=descOfTeam.getText().toString();
                final ProgressDialog pd = new ProgressDialog(Teams.this);
                pd.setCancelable(false);
                pd.setCanceledOnTouchOutside(false);
                pd.setMessage("Adding the user...");
                pd.show();
                StringRequest stringRequest = new StringRequest(Request.Method.POST, Constants.addTeam, new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        try {
                            jsonObject=new JSONObject(response);
                            int resp=jsonObject.getInt("resp");
                            String msg=jsonObject.getString("msg");
                            if(resp ==  1){
                                refresh();
                                pd.dismiss();
                                dialog.dismiss();

                            }else{
                                Toast.makeText(Teams.this, msg, Toast.LENGTH_SHORT).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                        new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {
                                pd.dismiss();
                                if (error instanceof TimeoutError || error instanceof NoConnectionError){
                                    Toast.makeText(Teams.this, "Please check your internet connection!", Toast.LENGTH_SHORT).show();
                                }
                            }
                        }
                ) {
                    protected Map<String, String> getParams() {
                        Map<String, String> params = new HashMap<>();
                        params.put("token", token);
                        params.put("user_id", userid);
                        params.put("team_name", name);
                        params.put("team_desc", desc);
                        return params;
                    }
                };
                RequestQueue requestQueue = Volley.newRequestQueue(Teams.this);
                requestQueue.add(stringRequest);
            }
        });



        dialog.show();
    }

//    @Override
//    public void onBackPressed() {
//        startActivity(new Intent(Teams.this, HomeActivity.class));
//        finish();
//    }

    @Override
    public void onClick(View v) {
        if (v == fab){
            addTeam();
        }
    }
}
