package com.company.trackify.helper;

import android.util.Log;

/**
 * Created by Omar on 16-03-2017.
 */

public class MemberEntries {
    String id;
    String team_id;
    String member_id;
    String name;
    String createdAt;
    String is_active;



    public MemberEntries(String id, String team_id, String member_id, String name, String createdAt, String is_active) {

        this.id = id;
        this.team_id = team_id;
        this.member_id = member_id;
        this.name = name;
        this.createdAt = createdAt;
        this.is_active = is_active;
    }

    public MemberEntries(){}
    public String getIs_active() {
        return is_active;
    }

    public void setIs_active(String is_active) {
        this.is_active = is_active;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTeam_id() {
        return team_id;
    }

    public void setTeam_id(String team_id) {
        this.team_id = team_id;
    }

    public String getMember_id() {
        return member_id;
    }

    public void setMember_id(String member_id) {
        this.member_id = member_id;
    }
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }
}
