package com.company.trackify.helper;

import java.math.BigInteger;

/**
 * Created by Omar on 19-03-2017.
 */

public class Validate {

    String Value = "";
    String Type = "";

    public static Boolean isValidMobile(String mobile){
        String ePattern = "^\\d{10}$";
        java.util.regex.Pattern p = java.util.regex.Pattern.compile(ePattern);
        java.util.regex.Matcher m = p.matcher(mobile);
        return m.matches();
    }

    public static boolean isValidEmailAddress(String email) {

        String ePattern = "^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@((\\[[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\])|(([a-zA-Z\\-0-9]+\\.)+[a-zA-Z]{2,}))$";
        java.util.regex.Pattern p = java.util.regex.Pattern.compile(ePattern);
        java.util.regex.Matcher m = p.matcher(email);
        return m.matches();

    }

    public static Boolean isValidUsername(String username){
        if(username.length() != 10){
            String ePattern = "^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@((\\[[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\])|(([a-zA-Z\\-0-9]+\\.)+[a-zA-Z]{2,}))$";
            java.util.regex.Pattern p = java.util.regex.Pattern.compile(ePattern);
            java.util.regex.Matcher m = p.matcher(username);
            return m.matches();
        }else{

            String ePattern = "^\\d{10}$";
            java.util.regex.Pattern p = java.util.regex.Pattern.compile(ePattern);
            java.util.regex.Matcher m = p.matcher(username);
            return m.matches();
        }
    }

    public static boolean isValidPassword(String password) {
        if(password.length() < 6){
            return false;
        }else{
            return true;
        }
    }
}
