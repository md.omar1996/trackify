package com.company.trackify.helper;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Omar on 16-03-2017.
 */

public class DatabaseHelper extends SQLiteOpenHelper {
    private static final String LOG = "anything";

    private static final int DATABASE_VERSION = 1;
    private static final String DATABASE_NAME = "trackify";

    private static final String TABLE_TEAM = "team";
    private static final String TABLE_MEMBER = "member";
    //common fields in team and member
    private static final String KEY_ID = "id";
    private static final String KEY_CREATED_AT = "created_dt";
    private static final String KEY_IS_ACTIVE = "is_active";
    //team's entries
    private static final String KEY_USER_ID = "user_id";
    private static final String KEY_TEAM_NAME = "team_name";
    private static final String KEY_TEAM_DESC = "team_desc";
    //member's entries
    private static final String KEY_TEAM_ID = "team_id";
    private static final String KEY_MEMBER_ID = "member_id";
    private static final String KEY_MEMBER_NAME = "name";

    //team table

    private static final String CREATE_TEAM_TABLE_ = "CREATE TABLE "
            + TABLE_TEAM + "(" + KEY_ID + " INTEGER PRIMARY KEY"+" "+"AUTOINCREMENT,"
            + KEY_USER_ID + " VARCHAR(10),"
            + KEY_TEAM_NAME + " VARCHAR(10),"
            + KEY_TEAM_DESC + " VARCHAR(70),"
            + KEY_CREATED_AT + " DATETIME,"
            + KEY_IS_ACTIVE + " VARCHAR(10)" + ")";

    //member table

    private static final String CREATE_MEMBER_TABLE = "CREATE TABLE "
            + TABLE_MEMBER + "(" + KEY_ID + " INTEGER PRIMARY KEY"+" "+"AUTOINCREMENT,"
            + KEY_TEAM_ID + " VARCHAR(10),"
            + KEY_MEMBER_ID + " VARCHAR(10),"
            + KEY_MEMBER_NAME + " VARCHAR(10),"
            + KEY_CREATED_AT + " DATETIME,"
            + KEY_IS_ACTIVE + " VARCHAR(10)" + ")";


    public DatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(CREATE_TEAM_TABLE_);
        db.execSQL(CREATE_MEMBER_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_MEMBER);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_TEAM);
        onCreate(db);
    }
    //for team
    public long createTeamEntry(TeamEntries team) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(KEY_ID, team.getId());
        values.put(KEY_USER_ID, team.getUser_id());
        values.put(KEY_TEAM_NAME, team.getTeam_name());
        values.put(KEY_TEAM_DESC, team.getTeam_desc());
        values.put(KEY_CREATED_AT, team.getCreatedAt());
        values.put(KEY_IS_ACTIVE, team.getIs_active());

        // insert row
        long team_entry_id = db.insert(TABLE_TEAM, null, values);
        return team_entry_id;

    }

    //for members
    public long createMemberEntry(MemberEntries member) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(KEY_ID, member.getId());
        values.put(KEY_TEAM_ID, member.getTeam_id());
        values.put(KEY_MEMBER_ID, member.getMember_id());
        values.put(KEY_MEMBER_NAME, member.getName());
        values.put(KEY_CREATED_AT, member.getCreatedAt());
        values.put(KEY_IS_ACTIVE, member.getIs_active());
        // insert row
        long member_entry_id = db.insert(TABLE_MEMBER, null, values);
        return member_entry_id;

    }

    public List<TeamEntries> getTeamEntries() {
        List<TeamEntries> userEntries = new ArrayList<TeamEntries>();
        String selectQuery = "SELECT  "+KEY_ID+","+KEY_USER_ID+","+KEY_TEAM_NAME+","+KEY_TEAM_DESC+","+KEY_CREATED_AT+","+KEY_IS_ACTIVE+" FROM " + TABLE_TEAM + " WHERE "+KEY_IS_ACTIVE+" = 1 ORDER BY "+ KEY_ID+ " ASC";

        Log.e(LOG, selectQuery);

        SQLiteDatabase db = this.getReadableDatabase();
        Cursor c = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (c.moveToFirst()) {
            do {
                TeamEntries ue = new TeamEntries();
                ue.setId(c.getString(c.getColumnIndex(KEY_ID)));
                ue.setUser_id((c.getString(c.getColumnIndex(KEY_USER_ID))));
                ue.setTeam_name((c.getString(c.getColumnIndex(KEY_TEAM_NAME))));
                ue.setTeam_desc((c.getString(c.getColumnIndex(KEY_TEAM_DESC))));
                ue.setCreatedAt(c.getString(c.getColumnIndex(KEY_CREATED_AT)));
                ue.setCreatedAt(c.getString(c.getColumnIndex(KEY_IS_ACTIVE)));
                // adding to Timetable list
                userEntries.add(ue);
            } while (c.moveToNext());
        }

        return userEntries;
    }
    public List<MemberEntries> getMemberEntries(String id) {
        List<MemberEntries> memberEntries = new ArrayList<MemberEntries>();
        String selectQuery = "SELECT  * FROM " + TABLE_MEMBER + " WHERE " + KEY_TEAM_ID + " = '" + id + "'";

        Log.e(LOG, selectQuery);

        SQLiteDatabase db = this.getReadableDatabase();
        Cursor c = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (c.moveToFirst()) {
            do {
                MemberEntries ue = new MemberEntries();
                ue.setId(c.getString(c.getColumnIndex(KEY_ID)));
                ue.setTeam_id((c.getString(c.getColumnIndex(KEY_TEAM_ID))));
                ue.setMember_id((c.getString(c.getColumnIndex(KEY_MEMBER_ID))));
                ue.setName((c.getString(c.getColumnIndex(KEY_MEMBER_NAME))));
                ue.setCreatedAt(c.getString(c.getColumnIndex(KEY_CREATED_AT)));
                ue.setIs_active(c.getString(c.getColumnIndex(KEY_IS_ACTIVE)));
                // adding to Timetable list
                memberEntries.add(ue);
            } while (c.moveToNext());
        }

        return memberEntries;
    }
    public List<MemberEntries> getAllMemberEntries(){
        List<MemberEntries> memberEntries = new ArrayList<MemberEntries>();
        String selectQuery = "SELECT DISTINCT (" + KEY_MEMBER_ID + "), " + KEY_MEMBER_NAME +
                " FROM " + TABLE_MEMBER + " ORDER BY "+ KEY_MEMBER_NAME+ " ASC";

        Log.e("get all members", selectQuery);

        SQLiteDatabase db = this.getReadableDatabase();
        Cursor c = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (c.moveToFirst()) {
            do {
                MemberEntries ue = new MemberEntries();
                ue.setMember_id((c.getString(c.getColumnIndex(KEY_MEMBER_ID))));
                ue.setName((c.getString(c.getColumnIndex(KEY_MEMBER_NAME))));
                memberEntries.add(ue);
            } while (c.moveToNext());
        }

        return memberEntries;
    }
    public void truncateTeam() {
        SQLiteDatabase db = this.getWritableDatabase();
        String selectQuery = "DELETE FROM " + TABLE_TEAM;
        Log.e(LOG, selectQuery);
        if (db != null && db.isOpen())
            db.execSQL(selectQuery);
    }
    public void truncateMember() {
        SQLiteDatabase db = this.getWritableDatabase();
        String selectQuery = "DELETE FROM " + TABLE_MEMBER;
        Log.e(LOG, selectQuery);
        if (db != null && db.isOpen())
            db.execSQL(selectQuery);
    }
}