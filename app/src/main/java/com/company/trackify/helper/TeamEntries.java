package com.company.trackify.helper;

/**
 * Created by Omar on 16-03-2017.
 */

public class TeamEntries {
    String id;
    String user_id;
    String team_name;
    String team_desc;
    String createdAt;
    String is_active;

    public TeamEntries(){

    }



    public TeamEntries(String id, String user_id, String team_name, String team_desc, String createdAt, String is_active) {
        this.id = id;
        this.user_id = user_id;
        this.team_name = team_name;
        this.team_desc = team_desc;
        this.createdAt = createdAt;
        this.is_active = is_active;

    }
    public String getIs_active() {
        return is_active;
    }

    public void setIs_active(String is_active) {
        this.is_active = is_active;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getTeam_name() {
        return team_name;
    }

    public void setTeam_name(String team_name) {
        this.team_name = team_name;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getTeam_desc() {
        return team_desc;
    }

    public void setTeam_desc(String team_desc) {
        this.team_desc = team_desc;
    }

}