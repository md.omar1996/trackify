package com.company.trackify.helper;

/**
 * Created by subodh on 15/3/17.
 */

public final class Constants {

    public final static String login = "https://agro-mycollege.c9users.io/api/login";
    public final static String changePass = "https://agro-mycollege.c9users.io/api/login/changepassword";
    public final static String addTeam = "https://agro-mycollege.c9users.io/api/team/createTeam";
    public final static String getTeam = "https://agro-mycollege.c9users.io/api/team/getTeam";
    public final static String setLocation = "https://agro-mycollege.c9users.io/api/member/setLocation";
    public final static String register = "https://agro-mycollege.c9users.io/api/signup";
    public final static String sync = "https://agro-mycollege.c9users.io/api/sync";
    public final static String addMember = "https://agro-mycollege.c9users.io/api/team/addMember";
    public static final String getMembers = "https://agro-mycollege.c9users.io/api/team/teamMembers";
    public static final String setSettings = "https://agro-mycollege.c9users.io/api/settings/setbasicsettings";
    public static final String setProfile = "https://agro-mycollege.c9users.io/api/profile/set";
    public static final String setProfilePhoto = "https://agro-mycollege.c9users.io/api/profile/photoUpload";
    public final static String deleteMember = "https://agro-mycollege.c9users.io/api/team/deleteMember";
    public final static String deleteTeam = "https://agro-mycollege.c9users.io/api/team/deleteTeam";



}
