package com.company.trackify;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.ContextMenu;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.company.trackify.CustomAdapters.CustomListAdapterForMember;
import com.company.trackify.CustomAdapters.CustomListAdapterForTeam;
import com.company.trackify.helper.Constants;
import com.company.trackify.helper.DatabaseHelper;
import com.company.trackify.helper.MemberEntries;
import com.company.trackify.helper.MySingleton;
import com.company.trackify.helper.TeamEntries;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
public class Members extends AppCompatActivity {
    ListView simpleList;
    RelativeLayout layout;
    String userid = "";
    String token="";
    String teamid="";
    String memberid="";
    String inviteCode="";
    SharedPreferences shared;
    JSONObject jsonObject;
    final String PREF_NAME = "USER";
    CustomListAdapterForMember adapter;
    DatabaseHelper db;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_members);
        layout=(RelativeLayout)findViewById(R.id.first_member);
        layout.setVisibility(View.GONE);
        simpleList=(ListView)findViewById(R.id.members_list);
        shared = getSharedPreferences(PREF_NAME, MODE_PRIVATE);
        userid = shared.getString("userid", "0");
        token = shared.getString("token", "0");
        inviteCode = shared.getString("invite_code","0");
        db = new DatabaseHelper(getApplicationContext());
        teamid = getIntent().getExtras().getString("DEVICEID");
        final List<MemberEntries> memberEntries = db.getMemberEntries(teamid);

        adapter = new CustomListAdapterForMember(Members.this, memberEntries);

        simpleList.setAdapter(adapter);

        registerForContextMenu(simpleList);


        if(adapter.isEmpty()){
            Log.e("Members","empty");
            layout.setVisibility(View.VISIBLE);
            Button button = (Button)layout.findViewById(R.id.add_member);
            button.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    addMember();
                }
            });
        }
        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);

        simpleList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
                    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                    try {
                        Log.e("test",Integer.toString(i));
                        MemberEntries memberEntries1= (MemberEntries) adapterView.getItemAtPosition(i);
                        String id =memberEntries1.getId();
                        Log.e("user id",id);
                        Intent intent = new Intent(getApplicationContext(),TrackActivity.class);
                        intent.putExtra("DEVICEID",memberEntries1.getId());
                        intent.putExtra("MEMBERID", memberEntries1.getMember_id());
                        intent.putExtra("MEMBER_NAME", memberEntries1.getName());
                        startActivity(intent);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    }
        });
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addMember();

    }

        });

    }
    public void addMember(){
        final Dialog dialog = new Dialog(Members.this);
        dialog.setContentView(R.layout.dialog_add_team);
        TextInputLayout textInputLayout = (TextInputLayout) dialog.findViewById(R.id.input_desc);
        textInputLayout.setVisibility(View.GONE);
        Button button_add=(Button)dialog.findViewById(R.id.btn_add);
        final TextInputEditText invitecode=(TextInputEditText) dialog.findViewById(R.id.name);
        invitecode.setHint("Enter your Invite Code");
        button_add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final String invite=invitecode.getText().toString();
                if (invite.equals("")){
                    Toast.makeText(Members.this, "Please enter the invite code!", Toast.LENGTH_SHORT).show();
                    return;
                }
                if (invite.equalsIgnoreCase(inviteCode)){
                    Toast.makeText(Members.this, "You cannot track your own self!", Toast.LENGTH_SHORT).show();
                    return;
                }
                final ProgressDialog pd = new ProgressDialog(Members.this);
                pd.setCancelable(false);
                pd.setCanceledOnTouchOutside(false);
                pd.setMessage("Adding the team member...");
                pd.show();
                StringRequest stringRequest = new StringRequest(Request.Method.POST, Constants.addMember, new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        refresh();
                        pd.dismiss();
                        dialog.dismiss();
                        try {
                            jsonObject=new JSONObject(response);
                            int resp=jsonObject.getInt("resp");
                            String msg=jsonObject.getString("msg");
                            if(resp ==  1){
                                layout.setVisibility(View.GONE);
                                Toast.makeText(Members.this, msg, Toast.LENGTH_SHORT).show();

                            }else{
                                Toast.makeText(Members.this, msg, Toast.LENGTH_SHORT).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                        new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {
                                pd.dismiss();
                                if (error instanceof TimeoutError || error instanceof NoConnectionError){
                                    Toast.makeText(Members.this, "Please check your internet connection!", Toast.LENGTH_SHORT).show();
                                }
                            }
                        }
                ) {
                    protected Map<String, String> getParams() {
                        Map<String, String> params = new HashMap<>();
                        params.put("user_id", userid);
                        params.put("token", token);
                        params.put("inviteCode", invite);
                        params.put("team_id", teamid);
                        return params;
                    }
                };
                MySingleton singleton=MySingleton.getInstance(getApplicationContext());
                singleton.getRequestQueue().add(stringRequest);
            }
        });
        dialog.show();
    }

    private void refresh(){
            final ProgressDialog pd = new ProgressDialog(Members.this);
            pd.setCancelable(false);
            pd.setCanceledOnTouchOutside(false);
            pd.setMessage("Refreshing.....");
            pd.show();
            StringRequest stringRequest = new StringRequest(Request.Method.POST, Constants.sync, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    pd.dismiss();
                    try {
                        jsonObject=new JSONObject(response);
                        int resp=jsonObject.getInt("resp");
                        Log.d("teams",response);
                        if (resp== 0){
                            String msg=jsonObject.getString("msg");
                            Toast.makeText(Members.this, msg, Toast.LENGTH_SHORT).show();
                        }else{
                            DatabaseHelper db;
                            db = new DatabaseHelper(getApplicationContext());
                            JSONArray teamArray = jsonObject.getJSONArray("team");
                            db.truncateTeam();
                            for(int i = 0; i < teamArray.length() ; i++ ){
                                String id = teamArray.getJSONObject(i).getString("id");
                                String user_id = teamArray.getJSONObject(i).getString("user_id");
                                String team_name = teamArray.getJSONObject(i).getString("team_name");
                                String team_desc = teamArray.getJSONObject(i).getString("team_desc");
                                String createdAt = teamArray.getJSONObject(i).getString("created_dt");
                                String is_active = teamArray.getJSONObject(i).getString("is_active");
                                TeamEntries userEntriesHelper=new TeamEntries(id, user_id, team_name, team_desc,createdAt,is_active);
                                Long ID = db.createTeamEntry(userEntriesHelper);
                                Log.d("LOG",ID.toString()+"hello world");
                            }
                            db.truncateMember();
                            JSONArray memberArray = jsonObject.getJSONArray("member");
                            for(int i = 0; i < memberArray.length() ; i++ ){
                                String id = memberArray.getJSONObject(i).getString("id");
                                String team_id = memberArray.getJSONObject(i).getString("team_id");
                                String member_id = memberArray.getJSONObject(i).getString("member_id");
                                String member_name = memberArray.getJSONObject(i).getString("name");
                                String createdAT = memberArray.getJSONObject(i).getString("created_dt");
                                String is_active = memberArray.getJSONObject(i).getString("is_active");
                                MemberEntries memberEntries = new MemberEntries(id, team_id, member_id, member_name, createdAT, is_active);
                                Long ID=db.createMemberEntry(memberEntries);
                                Log.d("LOG",ID.toString()+"hello omar");
                            }

                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    final List<MemberEntries> memberEntries = db.getMemberEntries(teamid);
                    adapter = new CustomListAdapterForMember(Members.this, memberEntries);
                    simpleList.setAdapter(adapter);
                }
            },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            pd.dismiss();
                            if (error instanceof TimeoutError || error instanceof NoConnectionError){
                                Toast.makeText(Members.this, "Please check your internet connection!", Toast.LENGTH_SHORT).show();
                            }
                        }
                    }
            ) {
                protected Map<String, String> getParams() {
                    Map<String, String> params = new HashMap<>();
                    params.put("user_id", userid);
                    params.put("token", token);
                    return params;
                }
            };
            MySingleton singleton=MySingleton.getInstance(getApplicationContext());
            singleton.getRequestQueue().add(stringRequest);
        }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        if (v.getId() == R.id.members_list) {
            ListView lv = (ListView) v;
            AdapterView.AdapterContextMenuInfo acmi = (AdapterView.AdapterContextMenuInfo) menuInfo;
            MemberEntries obj = (MemberEntries) lv.getItemAtPosition(acmi.position);
            memberid = obj.getMember_id();

            menu.add("Delete");
        }
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        if(item.getTitle()=="Delete"){
            deleteMember();
        }else{
            return false;
        }
        return true;
    }
    public void deleteMember(){
                final ProgressDialog pd = new ProgressDialog(Members.this);
                pd.setCancelable(false);
                pd.setCanceledOnTouchOutside(false);
                pd.setMessage("Deleting the team member...");
                pd.show();
                StringRequest stringRequest = new StringRequest(Request.Method.POST, Constants.deleteMember, new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        refresh();
                        pd.dismiss();
                        try {
                            jsonObject=new JSONObject(response);
                            int resp=jsonObject.getInt("resp");
                            String msg=jsonObject.getString("msg");
                            if(resp ==  1){
                                refresh();
                                Toast.makeText(Members.this, msg, Toast.LENGTH_SHORT).show();

                            }else{
                                Toast.makeText(Members.this, msg, Toast.LENGTH_SHORT).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                        new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {
                                pd.dismiss();
                                if (error instanceof TimeoutError || error instanceof NoConnectionError){
                                    Toast.makeText(Members.this, "Please check your internet connection!", Toast.LENGTH_SHORT).show();
                                }
                            }
                        }
                ) {
                    protected Map<String, String> getParams() {
                        Map<String, String> params = new HashMap<>();
                        params.put("user_id", userid);
                        params.put("token", token);
                        params.put("team_id", teamid);
                        params.put("member_id",memberid);
                        return params;
                    }
                };
                MySingleton singleton=MySingleton.getInstance(getApplicationContext());
                singleton.getRequestQueue().add(stringRequest);
            }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {

            // action with ID action_settings was selected
            case R.id.action_refresh:
                refresh();
                break;
            default:
                break;
        }
        return true;
    }

}