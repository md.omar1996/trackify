package com.company.trackify;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.media.Image;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.design.widget.TextInputEditText;
import android.support.v4.graphics.drawable.RoundedBitmapDrawable;
import android.support.v4.graphics.drawable.RoundedBitmapDrawableFactory;
import android.support.v7.app.AppCompatActivity;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.company.trackify.helper.Constants;
import com.company.trackify.helper.MySingleton;
import com.company.trackify.helper.Validate;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public class RegisterActivity extends AppCompatActivity implements View.OnClickListener{
    TextInputEditText email, password, name , mobile;
    TextView gotoLoginText;
    ImageButton gotoLogin, changeImage;
    private static final int FILE_SELECT_CODE = 101;
    ImageView iv;
    Button btn_register;
    int flag=1;
    JSONObject jsonObject;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        SharedPreferences shared = getSharedPreferences("USER", MODE_PRIVATE);
        String userid = shared.getString("userid","0");
        if(userid != "0")
        {
            Intent main = new Intent(RegisterActivity.this,HomeActivity.class);
            startActivity(main);
            finish();
        }

        setContentView(R.layout.test_signup);

        Resources res = getResources();
        Bitmap src = BitmapFactory.decodeResource(res, R.drawable.use_user);
        RoundedBitmapDrawable dr =
                RoundedBitmapDrawableFactory.create(res, src);
        dr.setCornerRadius(Math.max(src.getWidth(), src.getHeight()) / 2.0f);

        iv = (ImageView) findViewById(R.id.profileImage);
        iv.setImageDrawable(dr);

        email=(TextInputEditText)findViewById(R.id.email);
        password=(TextInputEditText)findViewById(R.id.password);
        name=(TextInputEditText)findViewById(R.id.name);
        mobile=(TextInputEditText)findViewById(R.id.mobile);
        btn_register=(Button)findViewById(R.id.btn_register);
        gotoLoginText = (TextView) findViewById(R.id.GotoLoginText);
        gotoLogin = (ImageButton) findViewById(R.id.GotoLogin);
        changeImage = (ImageButton) findViewById(R.id.changeImage);

        SpannableString text = new SpannableString("Already have an account? Sign In");
        text.setSpan(new ForegroundColorSpan(Color.BLACK), 25, 32, 0);
        gotoLoginText.setText(text, TextView.BufferType.SPANNABLE);

        changeImage.setOnClickListener(this);
        btn_register.setOnClickListener(this);
        gotoLogin.setOnClickListener(this);
        gotoLoginText.setOnClickListener(this);
    }
    public void register_user(){
        final String getEmail = email.getText().toString();
        if(!Validate.isValidEmailAddress(getEmail)){
            Toast.makeText(this, "Invalid email address", Toast.LENGTH_SHORT).show();
            return;
        }
        final String getMobile = mobile.getText().toString ();
        if(!Validate.isValidMobile(getMobile)){
            Toast.makeText(this, "Invalid mobile number", Toast.LENGTH_SHORT).show();
            return;
        }
        final String getName = name.getText().toString();
        if (getName.equals(" ")){
            Toast.makeText(this, "Invalid name", Toast.LENGTH_SHORT).show();
        }
        final String getPassword = password.getText().toString ();
        if(!Validate.isValidPassword(getPassword)){
            Toast.makeText(this, "Invalid password", Toast.LENGTH_SHORT).show();
            return;
        }
        if(flag==0){
            return;
        }else {
            flag=0;
        }
        final ProgressDialog pd = new ProgressDialog(RegisterActivity.this);
        pd.setCancelable(false);
        pd.setCanceledOnTouchOutside(false);
        pd.setMessage("Signing up...");
        pd.show();
        StringRequest stringRequest = new StringRequest(Request.Method.POST, Constants.register, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                flag=1;
                pd.dismiss();
                try {

                    jsonObject=new JSONObject(response);
                    int resp=jsonObject.getInt("resp");
                    if(resp ==  1){
                        Toast.makeText(RegisterActivity.this, "User successfully registered!", Toast.LENGTH_LONG).show();
                        Intent intent = new Intent(getApplicationContext(),LoginActivity.class);
                        startActivity(intent);
                        finish();

                    }else{

                        String msg = jsonObject.getString("msg");
                        Toast.makeText(RegisterActivity.this, msg, Toast.LENGTH_SHORT).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        flag=1;
                        pd.dismiss();
                        if (error instanceof TimeoutError || error instanceof NoConnectionError){
                            Toast.makeText(RegisterActivity.this, "Please check your internet connection!", Toast.LENGTH_SHORT).show();
                        }
                    }
                }
        ) {
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("name", getName);
                params.put("email", getEmail);
                params.put("mobile", getMobile);
                params.put("password", getPassword);
                return params;
            }
        };
        MySingleton singleton=MySingleton.getInstance(getApplicationContext());
        singleton.getRequestQueue().add(stringRequest);
    }

    @Override
    public void onClick(View v) {
        if (v == btn_register){
            register_user();
        }

        if(v == gotoLogin || v== gotoLoginText){
            gotoLogin();
        }

        if(v == changeImage){
            showFileChooser();
        }
    }

    private void gotoLogin() {
        Intent intent = new Intent(getApplicationContext(),LoginActivity.class);
        startActivity(intent);
        finish();
    }

    private void showFileChooser() {
        Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
        intent.setType("*/*");
        intent.addCategory(Intent.CATEGORY_OPENABLE);

        try {
            startActivityForResult(
                    Intent.createChooser(intent, "Select a Picture"),
                    FILE_SELECT_CODE);
        } catch (android.content.ActivityNotFoundException ex) {
            // Potentially direct the user to the Market with a Dialog
            Toast.makeText(this, "Please install a File Manager.",
                    Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case FILE_SELECT_CODE:
                if (resultCode == RESULT_OK) {
                    // Get the Uri of the selected file
                    Uri uri = data.getData();
                    Log.d("FILE URI", "File Uri: " + uri.toString());
                    try {
                        Bitmap bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), uri);
                        RoundedBitmapDrawable dr =
                                RoundedBitmapDrawableFactory.create(getResources(), bitmap);
                        dr.setCornerRadius(Math.max(bitmap.getWidth(), bitmap.getHeight()) / 2.0f);
                        iv.setImageDrawable(dr);

                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                    // Get the path
//                    String path = FileUtils.getPath(this, uri);
//                    Log.d(TAG, "File Path: " + path);
                    // Get the file instance
                    // File file = new File(path);
                    // Initiate the upload
                }
                break;
        }
        super.onActivityResult(requestCode, resultCode, data);
    }
}
