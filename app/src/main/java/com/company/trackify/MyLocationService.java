package com.company.trackify;

import android.Manifest;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.IBinder;
import android.support.v4.app.ActivityCompat;
import android.util.Log;
import android.widget.Toast;

import java.util.Timer;
import java.util.TimerTask;

/**
 * Created by Omar on 10-05-2017.
 */
public class MyLocationService extends Service {
    private Timer timer;
    private long UPDATE_INTERVAL;
    public static final String Stub = null;
    LocationManager mlocmag;
    LocationListener mlocList;
    private double lat, longn;

    @Override
    public void onCreate() {
        super.onCreate();
        Log.e("hello", "we are here");
        mlocmag = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        mlocList = new MyLocationList();

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        Location loc = mlocmag.getLastKnownLocation(LocationManager.GPS_PROVIDER);

        if (loc == null) {
            loc = mlocmag.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
            if (loc==null){
                Log.e("network provider", "couldn't get location from the net provider");

            }else {
                Log.e("network provider", loc.toString());

            }
        }else {
            Log.e("GPS", loc.toString());
        }
        timer  = new Timer();       // location.
        UpdateWithNewLocation(loc); // This method is used to get updated
        mlocmag.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0,mlocList);
    }

    @Override
    public IBinder onBind(Intent arg0) {
        return null;
    }



    @Override
    public void onDestroy() {
        super.onDestroy();
        if (timer != null) {
            timer.cancel();
        }
        mlocmag.removeUpdates(mlocList);
    }

    @Override
    public boolean stopService(Intent name) {
        return super.stopService(name);
    }

    private void UpdateWithNewLocation(final Location loc) {
//        final SharedPreferences prefs = getSharedPreferences(Const.COMMON_SHARED, Context.MODE_PRIVATE);
//        userId = prefs.getString(Const.COMMON_USERID, null);
//        gps = prefs.getInt(Const.COMMON_GPS, 0);

        UPDATE_INTERVAL = 60000;

        timer.scheduleAtFixedRate(new TimerTask() {
            @Override
            public void run() {
                if (loc != null) {
                    final double latitude = loc.getLatitude(); // Updated lat
                    final double longitude = loc.getLongitude(); // Updated long
                    Log.e("location service","called" + latitude);
                    String response = null ;
                    if (lat != latitude || longn != longitude ) {

//                        response = webService.updateLatandLong(userId, latitude, longitude);
                        lat = latitude;
                        longn = longitude;
//                        Toast.makeText(MyLocationService.this, lat + " " + longn , Toast.LENGTH_SHORT).show();

                    }
                }

                else {
                    String latLongStr = "No lat and longitude found";
                }

            }
        }, 0, UPDATE_INTERVAL);
    }


    public class MyLocationList implements LocationListener {

        public void onLocationChanged(Location arg0) {
            UpdateWithNewLocation(arg0);
        }

        public void onProviderDisabled(String provider) {
            Toast.makeText(getApplicationContext(), "GPS Disable ",
                    Toast.LENGTH_LONG).show();
        }

        public void onProviderEnabled(String provider) {
            Toast.makeText(getApplicationContext(), "GPS enabled",
                    Toast.LENGTH_LONG).show();
        }

        public void onStatusChanged(String provider, int status, Bundle extras) {

        }

    }
}