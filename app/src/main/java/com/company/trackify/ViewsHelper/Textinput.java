package com.company.trackify.ViewsHelper;

import android.content.Context;
import android.support.design.widget.TextInputEditText;
import android.util.AttributeSet;

/**
 * Created by mayank on 9/4/17.
 */

public class Textinput extends TextInputEditText {

    public Textinput(Context context) {
        super(context);
    }

    public Textinput(Context context, AttributeSet attrs) {
        super(context, attrs);
        CustomFontHelper.setCustomFont(this, context, attrs);
    }

    public Textinput(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        CustomFontHelper.setCustomFont(this, context, attrs);
    }
}