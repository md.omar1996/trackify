package com.company.trackify.ViewsHelper;

import android.content.Context;
import android.support.design.widget.TextInputEditText;
import android.util.AttributeSet;
import android.widget.EditText;

/**
 * Created by mayank on 9/4/17.
 */

public class EdittextInput extends TextInputEditText {

    public EdittextInput(Context context) {
        super(context);
    }

    public EdittextInput(Context context, AttributeSet attrs) {
        super(context, attrs);
        CustomFontHelper.setCustomFont(this, context, attrs);
    }

    public EdittextInput(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        CustomFontHelper.setCustomFont(this, context, attrs);
    }
}