package com.company.trackify.ViewsHelper;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.Button;

/**
 * Created by mayank on 9/4/17.
 */

public class ButtonLogin extends android.support.v7.widget.AppCompatButton {

    public ButtonLogin(Context context) {
        super(context);
    }

    public ButtonLogin(Context context, AttributeSet attrs) {
        super(context, attrs);
        CustomFontHelper.setCustomFont(this, context, attrs);
    }

    public ButtonLogin(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        CustomFontHelper.setCustomFont(this, context, attrs);
    }
}