package com.company.trackify.ViewsHelper;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.TextView;

/**
 * Created by mayank on 9/4/17.
 */

public class TextViewField extends android.support.v7.widget.AppCompatTextView {

    public TextViewField(Context context) {
        super(context);
        CustomFontHelper.setCustomFont(this, context, null);
    }

    public TextViewField(Context context, AttributeSet attrs) {
        super(context, attrs);
        System.out.print("working");
        CustomFontHelper.setCustomFont(this, context, attrs);
    }

    public TextViewField(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        System.out.print("working");
        CustomFontHelper.setCustomFont(this, context, attrs);
    }
}