package com.company.trackify;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.SwitchCompat;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.company.trackify.helper.Constants;
import com.company.trackify.helper.MySingleton;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

public class SettingsActivity extends AppCompatActivity implements View.OnClickListener{
    SwitchCompat switchCompat;
    TextView location_settings;
    Button set_settings;
    String distance= "50";
    String time = "600";
    String wifi="0";
    String userid="";
    String token="";
    JSONObject jsonObject;
    SharedPreferences shared;
    SharedPreferences preferences;
    final String PREF_NAME = "USER";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);
        switchCompat = (SwitchCompat)findViewById(R.id.toggleButton1);
        shared = getSharedPreferences(PREF_NAME, MODE_PRIVATE);
        wifi = shared.getString("wifi_only","0");
        Log.e("hello",wifi);
        if (wifi.equals("1")) //if (tgpref) may be enough, not sure
        {
            switchCompat.setChecked(true);
        }
        else
        {
            switchCompat.setChecked(false);
        }
        shared = getSharedPreferences(PREF_NAME, MODE_PRIVATE);
        userid = shared.getString("id","0");
        token = shared.getString("token","0");
        switchCompat.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked){
                    wifi="1";

                }else{
                    wifi="0";

                }
            }
        });
        location_settings = (TextView)findViewById(R.id.item3);
        set_settings = (Button)findViewById(R.id.btn_set) ;
        location_settings.setOnClickListener(this);
        set_settings.setOnClickListener(this);
    }
    public void showDialog(){
        final Dialog dialog =new Dialog(SettingsActivity.this);
        dialog.setContentView(R.layout.dialog_settings);
        dialog.setTitle("LOCATION SETTINGS");
        dialog.show();
        final Spinner forDistance=(Spinner)dialog.findViewById(R.id.accuracy_distance);
        final Spinner forTime=(Spinner)dialog.findViewById(R.id.accuracy_time);
        ArrayAdapter<CharSequence> adapterForDistance = ArrayAdapter.createFromResource(this,
                R.array.location_distance, android.R.layout.simple_spinner_item);
        adapterForDistance.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        forDistance.setAdapter(adapterForDistance);

        ArrayAdapter<CharSequence> adapterForTime = ArrayAdapter.createFromResource(this,
                R.array.location_time, android.R.layout.simple_spinner_item);
        adapterForTime.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        forTime.setAdapter(adapterForTime);
        Button btn_settings = (Button)dialog.findViewById(R.id.btn_settings);
        btn_settings.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                distance = forDistance.getSelectedItem().toString();
                String time1 = forTime.getSelectedItem().toString();
                int time2 = Integer.parseInt(time1)*60;
                time=String.valueOf(time2);
                dialog.dismiss();
            }
        });
    }
    public void setSettings(){
        Log.e("jello",userid + token + " " + distance + " " + time);
        final ProgressDialog pd = new ProgressDialog(SettingsActivity.this);
        pd.setCancelable(false);
        pd.setCanceledOnTouchOutside(false);
        pd.setMessage("Loading...");
        pd.show();
        StringRequest stringRequest = new StringRequest(Request.Method.POST, Constants.setProfile, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                pd.dismiss();
                try {

                    jsonObject=new JSONObject(response);
                    int resp=jsonObject.getInt("resp");
                    if(resp ==  1){
                        Toast.makeText(SettingsActivity.this, "Settings successfully saved!", Toast.LENGTH_LONG).show();
                        Intent intent = new Intent(getApplicationContext(),HomeActivity.class);
                        startActivity(intent);
                        finish();

                    }else{

                        String msg = jsonObject.getString("msg");
                        Toast.makeText(SettingsActivity.this, msg, Toast.LENGTH_SHORT).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                        pd.dismiss();
                        if (error instanceof TimeoutError || error instanceof NoConnectionError){
                            Toast.makeText(SettingsActivity.this, "Please check your internet connection!", Toast.LENGTH_SHORT).show();
                        }
                    }
                }
        ) {
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("user_id", userid);
                params.put("token", token);
                params.put("location_accuracy_distance", distance);
                params.put("location_accuracy_time", time);

                params.put("wifi_only", wifi);
                return params;
            }
        };
        MySingleton singleton=MySingleton.getInstance(getApplicationContext());
        singleton.getRequestQueue().add(stringRequest);
    }



    @Override
    public void onClick(View v) {
        if (v==location_settings) {
            showDialog();
        }else if (v == set_settings){
            setSettings();
        }

    }
}
