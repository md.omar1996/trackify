package com.company.trackify;

import android.Manifest;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.support.v4.app.ActivityCompat;
import android.util.Log;
import android.widget.Toast;

import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import java.util.HashMap;
import java.util.Map;

import static com.company.trackify.LocationService.SET_LOCATION;

public class NewLocationService extends Service {
    private static final String TAG = "new Location service";
    private LocationManager mLocationManager = null;
    private static int LOCATION_INTERVAL = 10 * 1000;
    private static int LOCATION_DISTANCE = 0;
    Handler handler;
    Location mLastLocation;
    public static String token = "";
    public static String user_id = "";
    public static int time = 5;
    public static int distance = 100;
    final String PREF_NAME = "USER";
    SharedPreferences shared;


    private class LocationListener implements android.location.LocationListener {


        public LocationListener(String provider) {
          //  Log.e(TAG, "LocationListener " + provider);
            mLastLocation = new Location(provider);
        }

        @Override
        public void onLocationChanged(Location location) {
        //    Log.e(TAG, "onLocationChanged: " + location);
            mLastLocation.set(location);
//            Log.e(TAG, Double.toString(mLastLocation.getLongitude()));
        }

        @Override
        public void onProviderDisabled(String provider) {
       //     Log.e(TAG, "onProviderDisabled: " + provider);
        }

        @Override
        public void onProviderEnabled(String provider) {
         //   Log.e(TAG, "onProviderEnabled: " + provider);
        }

        @Override
        public void onStatusChanged(String provider, int status, Bundle extras) {
         //   Log.e(TAG, "onStatusChanged: " + provider);
        }
    }

    LocationListener[] mLocationListeners = new LocationListener[]{
            new LocationListener(LocationManager.GPS_PROVIDER),
            new LocationListener(LocationManager.NETWORK_PROVIDER)
    };

    @Override
    public IBinder onBind(Intent arg0) {
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        //Log.e(TAG, "onStartCommand");
        super.onStartCommand(intent, flags, startId);
        return START_STICKY;
    }

    @Override
    public void onCreate() {

      //  Log.e(TAG, "onCreate");
        initializeLocationManager();
        try {
            mLocationManager.requestLocationUpdates(
                    LocationManager.NETWORK_PROVIDER, LOCATION_INTERVAL, LOCATION_DISTANCE,
                    mLocationListeners[1]);
        } catch (java.lang.SecurityException ex) {
            Log.i(TAG, "fail to request location update, ignore", ex);
        } catch (IllegalArgumentException ex) {
            Log.d(TAG, "network provider does not exist, " + ex.getMessage());
        }
        try {
            mLocationManager.requestLocationUpdates(
                    LocationManager.GPS_PROVIDER, LOCATION_INTERVAL, LOCATION_DISTANCE,
                    mLocationListeners[0]);
        } catch (java.lang.SecurityException ex) {
            Log.i(TAG, "fail to request location update, ignore", ex);
        } catch (IllegalArgumentException ex) {
            Log.d(TAG, "gps provider does not exist " + ex.getMessage());
        }
        handler = new Handler();
        handler.postDelayed(new Runnable() {

            @Override
            public void run() {
                //Log.e("location", "restart");
                shared = getSharedPreferences(PREF_NAME, MODE_PRIVATE);
                user_id = shared.getString("userid", "0");
                token = shared.getString("token", "0");
//                time = shared.getInt("time", 0);
                distance = shared.getInt("distance", 0);
                LOCATION_INTERVAL = time * 1000;
                LOCATION_DISTANCE = distance;
                //Log.e("new location service", LOCATION_DISTANCE + "  " + LOCATION_INTERVAL);

                StringRequest stringRequest = new StringRequest(Request.Method.POST, SET_LOCATION, new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        //Log.e("hit",response);
                        //Toast.makeText(LocationService.this, latitude+" "+longitude+ " "+ response,  Toast.LENGTH_SHORT).show();
                    }
                },
                        new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {
                                if (error instanceof TimeoutError || error instanceof NoConnectionError){
                                    Log.e("new location service", "error in internet connection");
                                //    Toast.makeText(NewLocationService.this, "Please check your internet connection!", Toast.LENGTH_SHORT).show();
                                }
                            }
                        }
                ) {
                    protected Map<String, String> getParams() {
                        Map<String, String> params = new HashMap<>();
                        params.put("user_id", user_id);
                        params.put("token", token);
                        params.put("lat", Double.toString(mLastLocation.getLatitude()));
                        params.put("lng", Double.toString(mLastLocation.getLongitude()));
                        return params;
                    }
                };
                RequestQueue requestQueue = Volley.newRequestQueue(NewLocationService.this);
                requestQueue.add(stringRequest);
                handler.postDelayed(this, LOCATION_INTERVAL);

            }
        }, 5000);
    }

    @Override
    public void onDestroy() {
        Log.e(TAG, "onDestroy");
        super.onDestroy();
        if (mLocationManager != null) {
            for (int i = 0; i < mLocationListeners.length; i++) {
                try {
                    mLocationManager.removeUpdates(mLocationListeners[i]);
                } catch (Exception ex) {
                    Log.i(TAG, "fail to remove location listners, ignore", ex);
                }
            }
        }
    }

    private void initializeLocationManager() {
        Log.e(TAG, "initializeLocationManager");
        if (mLocationManager == null) {
            mLocationManager = (LocationManager) getApplicationContext().getSystemService(Context.LOCATION_SERVICE);
            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                // TODO: Consider calling
                //    ActivityCompat#requestPermissions
                // here to request the missing permissions, and then overriding
                //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                //                                          int[] grantResults)
                // to handle the case where the user grants the permission. See the documentation
                // for ActivityCompat#requestPermissions for more details.
                return;
            }
            if (mLocationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER)!=null){
                mLastLocation = mLocationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
            }else if (mLocationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER) !=null){
                mLastLocation = mLocationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
            }

        }
    }
}