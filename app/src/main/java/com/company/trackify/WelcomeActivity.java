package com.company.trackify;

import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;

public class WelcomeActivity extends AppCompatActivity {
    RelativeLayout relativeLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        SharedPreferences shared = getSharedPreferences("USER", MODE_PRIVATE);
        String userid = shared.getString("id","0"); // // TODO: 05-04-2017 convert to imt  Shared pref UserID
        if(userid != "0") // this will automatically get fixed..okay
        {
            Intent main = new Intent(WelcomeActivity.this,HomeActivity.class);
            startActivity(main);
        }
        setContentView(R.layout.activity_welcome);
        relativeLayout = (RelativeLayout) findViewById(R.id.activity_welcome);
        ImageView myImageView= (ImageView)findViewById(R.id.image1);
        Animation myFadeInAnimation = AnimationUtils.loadAnimation(this, R.anim.fadein);
        // TODO: 05-04-2017 Ask subodh i created many animations  please import them in this project
        myImageView.startAnimation(myFadeInAnimation);

        Button button_login, button_signup;

        button_login=(Button)findViewById(R.id.btn_signin);
        button_signup=(Button)findViewById(R.id.btn_signup);

        //Please use a standard method of on click that is by implementing onclicklistener you know better... low priority
        button_login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(WelcomeActivity.this,LoginActivity.class));
            }
        });

        button_signup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(WelcomeActivity.this,RegisterActivity.class));
            }
        });

    }
}
