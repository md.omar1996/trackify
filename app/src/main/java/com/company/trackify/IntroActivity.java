package com.company.trackify;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Html;
import android.text.Layout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

public class IntroActivity extends AppCompatActivity {

    ViewPager viewPager;
    LinearLayout dotsLayout;
    int[] layouts;
    MyViewPagerAdapter myViewPagerAdapter;
    TextView[] dots;
    Button nextBtn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        SharedPreferences shared = getSharedPreferences("USER", MODE_PRIVATE);
        String userid = shared.getString("userid","0");
        if(userid != "0")
        {
            Intent main = new Intent(IntroActivity.this,ProfileFinal.class);
            startActivity(main);
            finish();
        }
        setContentView(R.layout.test_intro);
        dotsLayout = (LinearLayout) findViewById(R.id.dotsLayout);
        nextBtn = (Button) findViewById(R.id.nextBtn);

        layouts = new int[]{
                R.layout.introslide_1,
                R.layout.introslide_2,
                R.layout.introslide_3
        };

        viewPager = (ViewPager) findViewById(R.id.viewpager);
        myViewPagerAdapter = new MyViewPagerAdapter();
        viewPager.setPageTransformer(false, pageTransformer);
        viewPager.setAdapter(myViewPagerAdapter);
        addBottomDots(0);

        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
            }

            @Override
            public void onPageSelected(int position) {
                addBottomDots(position);

                if(position == layouts.length -1){
                    nextBtn.setText("DONE");
                }else{
                    nextBtn.setText("NEXT");
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {
            }
        });

        nextBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int item = viewPager.getCurrentItem();
                if(item == layouts.length - 1){
                    Intent i = new Intent(IntroActivity.this, LoginActivity.class);
                    startActivity(i);
                    finish();
                }else{
                    viewPager.setCurrentItem(item+1);
                }
            }
        });
    }

    private void addBottomDots(int currentPage) {
        dots = new TextView[layouts.length];

        int[] colorsActive = getResources().getIntArray(R.array.array_dot_active);
        int[] colorsInactive = getResources().getIntArray(R.array.array_dot_inactive);

        dotsLayout.removeAllViews();
        for (int i = 0; i < dots.length; i++) {
            dots[i] = new TextView(this);
            dots[i].setText(Html.fromHtml("&#8226;"));
            dots[i].setTextSize(35);
            dots[i].setTextColor(Color.LTGRAY);
            dotsLayout.addView(dots[i]);
        }

        if (dots.length > 0)
            dots[currentPage].setTextColor(Color.DKGRAY);
    }

    public class MyViewPagerAdapter extends PagerAdapter{

        private LayoutInflater layoutInflater;

        public MyViewPagerAdapter(){

        }

        @Override
        public Object instantiateItem(ViewGroup container, int position) {
            layoutInflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);

            View view = layoutInflater.inflate(layouts[position], container, false);
            container.addView(view);

            return view;
        }

        @Override
        public int getCount() {
            return layouts.length;
        }

        @Override
        public boolean isViewFromObject(View view, Object object) {
            return view == object;
        }

        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            View view = (View) object;
            container.removeView(view);
        }
    }

    ViewPager.PageTransformer pageTransformer = new ViewPager.PageTransformer() {
        @Override
        public void transformPage(View page, float position) {
            page.setTranslationX(page.getWidth() * -position);

            if(position <= -1.0F || position >= 1.0F) {
                page.setAlpha(0.0F);
            } else if( position == 0.0F ) {
                page.setAlpha(1.0F);
            } else {
                // position is between -1.0F & 0.0F OR 0.0F & 1.0F
                page.setAlpha(1.0F - Math.abs(position));
            }
        }
    };
}
