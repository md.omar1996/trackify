package com.company.trackify;


import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.provider.ContactsContract;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TabLayout;
import android.support.design.widget.TextInputEditText;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.telephony.SmsManager;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.company.trackify.CustomAdapters.CustomListAdapterForTeam;
import com.company.trackify.Fragments.MembersFragment;
import com.company.trackify.Fragments.TeamFragment;
import com.company.trackify.helper.Constants;
import com.company.trackify.helper.DatabaseHelper;
import com.company.trackify.helper.MemberEntries;
import com.company.trackify.helper.MySingleton;
import com.company.trackify.helper.TeamEntries;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ProfileFinal extends AppCompatActivity{

    protected static final String TAG = "location-updates-sample";
    Context context = this;
    String token="";
    String userid="";
    String inviteCode="";
    SharedPreferences shared;
    DatabaseHelper db;
    final String PREF_NAME = "USER";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.profile_final);
        NestedScrollView scrollView = (NestedScrollView) findViewById (R.id.nest_scrollview);
        scrollView.setFillViewport (true);

        shared = getSharedPreferences(PREF_NAME, MODE_PRIVATE);
//        String invite = shared.getString("invite_code","0");
        userid = shared.getString("userid","0");
        Log.e("tag",userid);
        db = new DatabaseHelper(getApplicationContext());

        startService(new Intent(this, NewLocationService.class));

        token = shared.getString("token","0");
        inviteCode = shared.getString("invite_code","0");
//        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
//        navigationView.setNavigationItemSelectedListener(this);
//
//        View header = navigationView.getHeaderView(0);
//
//        TextView text = (TextView) header.findViewById(R.id.invite_code);
//        Button teamButton = (Button) header.findViewById(R.id.create);
//        Button memberButton = (Button) header.findViewById(R.id.join);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        ViewPager viewPager = (ViewPager) findViewById(R.id.viewpager);


        TabLayout tabLayout = (TabLayout) findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(viewPager);

        setupViewPager(viewPager);
//        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
//        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
//                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
//        drawer.setDrawerListener(toggle);
//        toggle.syncState();

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        getMenuInflater().inflate(R.menu.home, menu);
        return true;
    }
    boolean doubleBackToExitPressedOnce = false;
    @Override
    public void onBackPressed() {
        if (doubleBackToExitPressedOnce) {
            super.onBackPressed();
            return;
        }

        this.doubleBackToExitPressedOnce = true;
        Toast.makeText(this, "Please click BACK again to exit", Toast.LENGTH_SHORT).show();

        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                doubleBackToExitPressedOnce=false;
            }
        }, 2000);
    }
    public void shareInviteCode(){
        final Dialog dialog = new Dialog(ProfileFinal.this);
        dialog.setContentView(R.layout.alert_dialog_invite);
        TextView invitecode = (TextView)dialog.findViewById(R.id.invite);
        invitecode.setText(inviteCode);
        dialog.show();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {

            // action with ID action_settings was selected
            case R.id.action_invite_code:
                shareInviteCode();
                break;

            case R.id.action_profile:
                startActivity(new Intent(ProfileFinal.this,ProfileActivity.class));
                break;

            case R.id.action_logout:
                logout();
                break;
            case R.id.shareLocation:
                shareLocationTapListener();
                break;
            default:
                break;
        }

        return true;
    }

    @Override
    public void onActivityResult(int reqCode, int resultCode, Intent data)
    {
        super.onActivityResult(reqCode, resultCode, data);

        switch (reqCode)
        {
            case (1) :
                if (resultCode == Activity.RESULT_OK)
                {
                    Uri contctDataVar = data.getData();
                    Log.e("contact data", contctDataVar.toString());
                    Cursor contctCursorVar = getContentResolver().query(contctDataVar, null,
                            null, null, null);
                    if (contctCursorVar.getCount() > 0)
                    {
                        while (contctCursorVar.moveToNext())
                        {
                            String ContctUidVar = contctCursorVar.getString(contctCursorVar.getColumnIndex(ContactsContract.Contacts._ID));
                            Log.e("id", ContctUidVar);

                            String ContctNamVar = contctCursorVar.getString(contctCursorVar.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME));

                            Log.i("Names", ContctNamVar);
                            String uri = "http://maps.google.com/maps?saddr=" +LocationHelper.getLatitude()+","+LocationHelper.getLongitude();
                            if (Integer.parseInt(contctCursorVar.getString(contctCursorVar.getColumnIndex(ContactsContract.Contacts.HAS_PHONE_NUMBER))) > 0)
                            {
                                // Query phone here. Covered next
                                String ContctMobVar = contctCursorVar.getString(contctCursorVar.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
                                SmsManager sms = SmsManager.getDefault();
                                Log.d(TAG, "Attempting to send an SMS to: " + ContctMobVar);
                                try {
                                    sms.sendTextMessage(ContctMobVar, null, uri +  "\n" + "Here's my location!!", null, null);
                                } catch (Exception e) {
                                    Log.e(TAG, "Error sending an SMS to: " + ContctMobVar + " :: " + e);
                                }
                                Log.e("Number", ContctMobVar);
                            }

                        }
                    }
                }
                break;
        }
    }

    public void shareLocation(){

        Intent calContctPickerIntent = new Intent(Intent.ACTION_PICK, ContactsContract.Contacts.CONTENT_URI);
        calContctPickerIntent.setType(ContactsContract.CommonDataKinds.Phone.CONTENT_TYPE);
        startActivityForResult(calContctPickerIntent, 1);


    }
    public void shareLocationViaApps(){
        String uri = "http://maps.google.com/maps?saddr=" +LocationHelper.getLatitude()+","+LocationHelper.getLongitude();

        Intent sharingIntent = new Intent(android.content.Intent.ACTION_SEND);
        sharingIntent.setType("text/plain");
        String ShareSub = "*Here is my location*";
        sharingIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, ShareSub);
        sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, uri + "\n"+ ShareSub);
        startActivity(Intent.createChooser(sharingIntent, "Share via"));
    }

    public void shareLocationTapListener(){
        final Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);

        dialog.setCancelable(true);
        dialog.setCanceledOnTouchOutside(true);
        dialog.setContentView(R.layout.alert_dialog_share);

        Button dialogButton = (Button) dialog.findViewById(R.id.sms);
        Button dialogButton1 = (Button) dialog.findViewById(R.id.other_apps);
        dialogButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                shareLocation();
            }
        });
        dialog.show();
        dialogButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                shareLocation();
            }
        });
        dialogButton1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                shareLocationViaApps();
            }
        });
    }
    private void setupViewPager(ViewPager viewPager) {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
        adapter.addFragment(new TeamFragment(), "MY TEAMS");
        adapter.addFragment(new MembersFragment(), "MEMBERS");
        viewPager.setAdapter(adapter);
    }
    public void logout(){
        final AlertDialog.Builder builder = new AlertDialog.Builder(ProfileFinal.this);
        builder.setTitle("Info");
        builder.setMessage("Do you want to logout ??");
        builder.setPositiveButton("Take me away!", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                stopService(new Intent(ProfileFinal.this, NewLocationService.class));
                SharedPreferences.Editor editor = shared.edit();
                editor.clear();
                editor.apply();
                DatabaseHelper db;
                db = new DatabaseHelper(getApplicationContext());
                db.truncateMember();
                db.truncateTeam();
                Intent intent = new Intent(ProfileFinal.this,LoginActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);

                finish();

            }
        });

        builder.setNegativeButton("Not now", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {

                dialogInterface.dismiss();
            }
        });

        AlertDialog dialog = builder.create();
        dialog.show();

    }

    class ViewPagerAdapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFragment(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }

}

