package com.company.trackify;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Handler;
import android.provider.ContactsContract;
import android.support.design.widget.TextInputEditText;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.company.trackify.CustomAdapters.CustomListAdapterForTeam;
import com.company.trackify.helper.Constants;
import com.company.trackify.helper.DatabaseHelper;
import com.company.trackify.helper.MemberEntries;
import com.company.trackify.helper.MySingleton;
import com.company.trackify.helper.TeamEntries;
import com.company.trackify.helper.Validate;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.company.trackify.LocationService.token;

public class LoginActivity extends AppCompatActivity implements View.OnClickListener {
    String action = "login";
    TextInputEditText usernameInput, passwordInput;
    Button login;
    Button backtologin;
    JSONObject jsonObject;
    String SHARED_PREF_KEY = "USER";
    int flag=1;
    DatabaseHelper db;
    String userid="";
    String token;
    TextView gotoSignup;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        SharedPreferences shared = getSharedPreferences("USER", MODE_PRIVATE);
        String userid = shared.getString("userid","0");
        if(userid != "0")
        {
            Intent main = new Intent(LoginActivity.this,ProfileFinal.class);
            startActivity(main);
            finish();
        }
        setContentView(R.layout.test_login);
        login = (Button) findViewById(R.id.btn_login);
        usernameInput = (TextInputEditText)findViewById(R.id.username);
        passwordInput = (TextInputEditText)findViewById(R.id.password);
        db = new DatabaseHelper(getApplicationContext());
        login.setOnClickListener(this);

        gotoSignup = (TextView) findViewById(R.id.GotoSignup);
        SpannableString text = new SpannableString("Don't have an account? Sign Up");
        text.setSpan(new ForegroundColorSpan(Color.BLACK), 23, 30, 0);
        gotoSignup.setText(text, TextView.BufferType.SPANNABLE);

        gotoSignup.setOnClickListener(this);

    }

    public void forgotpassword(){
        final String username = usernameInput.getText().toString();
        if(!Validate.isValidUsername(username)){
            Toast.makeText(this, "Invalid username", Toast.LENGTH_SHORT).show();
            return;
        }

        if(flag==0){
            return;
        }else {
            flag=0;
        }

        final ProgressDialog pd = new ProgressDialog(LoginActivity.this);
        pd.setCancelable(false);
        pd.setCanceledOnTouchOutside(false);
        pd.setMessage("Validating...");
        pd.show();

        StringRequest stringRequest = new StringRequest(Request.Method.POST, Constants.changePass, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                flag=1;
                pd.dismiss();
                try {

                    jsonObject=new JSONObject(response);
                    int resp=jsonObject.getInt("resp");
                    if(resp ==  1){

                        Toast.makeText(LoginActivity.this, response, Toast.LENGTH_SHORT).show();
                    }else{

                        String msg = jsonObject.getString("msg");
                        Toast.makeText(LoginActivity.this, msg, Toast.LENGTH_SHORT).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        flag=1;
                        pd.dismiss();
                        if (error instanceof TimeoutError || error instanceof NoConnectionError){
                            Toast.makeText(LoginActivity.this, "Please check your internet connection!", Toast.LENGTH_SHORT).show();
                        }
                    }
                }
        ) {
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("username", username);
                return params;
            }
        };
        MySingleton singleton=MySingleton.getInstance(getApplicationContext());
        singleton.getRequestQueue().add(stringRequest);
    }

    public void refresh(){
        final ProgressDialog pd = new ProgressDialog(LoginActivity.this);
        pd.setCancelable(false);
        pd.setCanceledOnTouchOutside(false);
        pd.setMessage("Refreshing.....");
        pd.show();
        StringRequest stringRequest = new StringRequest(Request.Method.POST, Constants.sync, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                pd.dismiss();
                Log.e("refresh", response);
                try {
                    jsonObject=new JSONObject(response);
                    int resp=jsonObject.getInt("resp");

                    SharedPreferences sharedpreferences = getSharedPreferences(SHARED_PREF_KEY, Context.MODE_PRIVATE);
                    SharedPreferences.Editor editor = sharedpreferences.edit();
                    editor.putInt("time", jsonObject.getInt("time"));
                    editor.putInt("distance",jsonObject.getInt("distance"));
                    editor.apply();

                    Log.d("teams",response);
                    if (resp == 0){
                        String msg=jsonObject.getString("msg");
                        Toast.makeText(LoginActivity.this, msg, Toast.LENGTH_SHORT).show();
                    }else{
                        DatabaseHelper db;
                        db = new DatabaseHelper(getApplicationContext());
                        db.truncateTeam();
                        if(jsonObject.getString("team") != "false"){
                            JSONArray teamArray = jsonObject.getJSONArray("team");
                            for(int i = 0; i < teamArray.length() ; i++ ){
                                Log.e("team",teamArray.getJSONObject(i).toString() );
                                String id = teamArray.getJSONObject(i).getString("id");
                                String user_id = teamArray.getJSONObject(i).getString("user_id");
                                String team_name = teamArray.getJSONObject(i).getString("team_name");
                                String team_desc = teamArray.getJSONObject(i).getString("team_desc");
                                String createdAt = teamArray.getJSONObject(i).getString("created_dt");
                                String is_active = teamArray.getJSONObject(i).getString("is_active");
                                TeamEntries userEntriesHelper=new TeamEntries(id, user_id, team_name, team_desc,createdAt,is_active);
                                Long ID = db.createTeamEntry(userEntriesHelper);
                                Log.d("LOG",ID.toString()+"hello world");
                                Log.e("LOfadfG",id+" hello world "+ user_id);
                            }
                        }


                        db.truncateMember();
                        if(jsonObject.getString("member") != "false"){
                            JSONArray memberArray = jsonObject.getJSONArray("member");
                            for(int i = 0; i < memberArray.length() ; i++ ){
                                Log.e("member",memberArray.getJSONObject(i).toString() );
                                String id = memberArray.getJSONObject(i).getString("id");
                                String team_id = memberArray.getJSONObject(i).getString("team_id");
                                String member_id = memberArray.getJSONObject(i).getString("member_id");
                                String name = memberArray.getJSONObject(i).getString("name");
                                String createdAT = memberArray.getJSONObject(i).getString("created_dt");
                                String is_active = memberArray.getJSONObject(i).getString("is_active");
                                MemberEntries memberEntries = new MemberEntries(id, team_id, member_id, name, createdAT, is_active);
                                Long ID=db.createMemberEntry(memberEntries);
                                Log.d("LOG",ID.toString()+"hello omar");
                                Log.d("LOG",name+"hello omar");

                            }

                        }

                        //starting home activity if login successful
                        Intent intent = new Intent(getApplicationContext(),ProfileFinal.class);
                        startActivity(intent);
                        finish();

                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        pd.dismiss();
                        if (error instanceof TimeoutError || error instanceof NoConnectionError){
                            Toast.makeText(LoginActivity.this, "Please check your internet connection!", Toast.LENGTH_SHORT).show();
                        }
                    }
                }
        ) {
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("user_id", userid);
                params.put("token", token);
                return params;
            }
        };
        MySingleton singleton=MySingleton.getInstance(getApplicationContext());
        singleton.getRequestQueue().add(stringRequest);
    }

    public void signin(){

        final String username = usernameInput.getText().toString();
        if(!Validate.isValidUsername(username)){
            Toast.makeText(this, "Invalid username", Toast.LENGTH_SHORT).show();
            return;
        }

        final String password= passwordInput.getText().toString();
        if(!Validate.isValidPassword(password)){
            Toast.makeText(this, "Invalid password", Toast.LENGTH_SHORT).show();
            return;
        }

        if(flag==0){
            return;
        }else {
            flag=0;
        }

        final ProgressDialog pd = new ProgressDialog(LoginActivity.this);
        pd.setCancelable(false);
        pd.setCanceledOnTouchOutside(false);
        pd.setMessage("Signing in...");
        pd.show();

        StringRequest stringRequest = new StringRequest(Request.Method.POST, Constants.login, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.e("test", "hello world");
                flag=1;
                pd.dismiss();
                try {

                    jsonObject=new JSONObject(response);

                    int resp=jsonObject.getInt("resp");
                    if(resp ==  1){
                        SharedPreferences sharedpreferences = getSharedPreferences(SHARED_PREF_KEY, Context.MODE_PRIVATE);
                        SharedPreferences.Editor editor = sharedpreferences.edit();
                        JSONObject jo=new JSONObject(jsonObject.getString("user"));
                        //storing all info in shared preferences
                        editor.putString("token",jsonObject.getString("token"));
                        editor.putString("userid",jo.getString("id"));
                        editor.putString("name",jo.getString("name"));
                        editor.putString("mobile",jo.getString("mobile"));
                        editor.putString("address",jo.getString("address"));
                        editor.putString("state",jo.getString("state"));
                        editor.putString("country",jo.getString("country"));
                        editor.putString("invite_code",jo.getString("invite_code"));
                        editor.putString("location_accuracy_time",jo.getString("location_accuracy_time"));
                        editor.putString("location_accuracy_distance",jo.getString("location_accuracy_distance"));
                        editor.putString("wifi_only",jo.getString("wifi_only"));
                        editor.putString("show_hospital",jo.getString("show_hospital"));
                        editor.putString("show_police_station",jo.getString("show_police_station"));
                        editor.putString("show_petrol_pump",jo.getString("show_petrol_pump"));
                        editor.putString("show_mechanic",jo.getString("show_mechanic"));
                        editor.putString("show_hangout_places",jo.getString("show_hangout_places"));
                        editor.apply();

                        userid=jo.getString("id");
                        Log.e("login",userid);
                        token=jsonObject.getString("token");
                        refresh();


                    }else{
                        //handling unsuccessful login
                        String msg = jsonObject.getString("msg");
                        Toast.makeText(LoginActivity.this, msg, Toast.LENGTH_SHORT).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        },
                //volley check
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        flag=1;
                        pd.dismiss();
                        if (error instanceof TimeoutError || error instanceof NoConnectionError){
                            Toast.makeText(LoginActivity.this, "Please check your internet connection!", Toast.LENGTH_SHORT).show();
                        }
                    }
                }
        ) {
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("username", username);
                params.put("password", password);
                return params;
            }
        };
        MySingleton singleton=MySingleton.getInstance(getApplicationContext());
        singleton.getRequestQueue().add(stringRequest);
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                15 * 1000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
    }

    public void onClick(View v) {
        if (v==backtologin){
            if(action.equalsIgnoreCase("login")){
                login.setText("CHANGE PASSWORD");
                passwordInput.setVisibility(View.GONE);
                backtologin.setText("Back to login?");
                action = "forgot";
            }else{
                action = "login";
                login.setText("LOGIN");
                passwordInput.setVisibility(View.VISIBLE);
                backtologin.setText("Forgot password?");

            }

        }

        if(v == login){
            if(action.equalsIgnoreCase("login")) {
                signin();
            }else{
                forgotpassword();
            }
        }

        if(v == gotoSignup){
            gotoSignup();
        }
    }

    private void gotoSignup() {
        Intent intent = new Intent(getApplicationContext(),RegisterActivity.class);
        startActivity(intent);
        finish();
    }
}
