package com.company.trackify;

import android.location.Location;
import android.util.Log;

/**
 * Created by Omar on 13-06-2017.
 */

public class LocationHelper {
    private static double latitude,longitude;
    public LocationHelper(){
    }

    public LocationHelper(double latitude, double longitude) {
        this.latitude = latitude;
        this.longitude = longitude;
    }

    public static double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;

    }

    public static double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }
}
