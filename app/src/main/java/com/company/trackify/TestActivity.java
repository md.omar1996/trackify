package com.company.trackify;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.icu.util.Calendar;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.DialogFragment;
import android.support.v4.graphics.drawable.RoundedBitmapDrawable;
import android.support.v4.graphics.drawable.RoundedBitmapDrawableFactory;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.WindowManager;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

public class TestActivity extends AppCompatActivity {

    static EditText edittext_birthday;
    TextInputLayout birthday;
    private int mYear, mMonth, mDay, mHour, mMinute;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        setContentView(R.layout.test_settings);
        edittext_birthday = (EditText)findViewById(R.id.edittext_birthday);

        Resources res = getResources();
        Bitmap src = BitmapFactory.decodeResource(res, R.drawable.image_signup);
        RoundedBitmapDrawable dr =
                RoundedBitmapDrawableFactory.create(res, src);
        dr.setCornerRadius(Math.max(src.getWidth(), src.getHeight()) / 2.0f);

        ImageView iv = (ImageView) findViewById(R.id.circularImage);
        iv.setImageDrawable(dr);
//
//        TextView textView = (TextView) findViewById(R.id.BacktoSigninText);
//        SpannableString text = new SpannableString("DON'T HAVE AN ACCOUNT? SIGN UP");
//        text.setSpan(new ForegroundColorSpan(Color.BLACK), 23, 30, 0);
//        textView.setText(text, TextView.BufferType.SPANNABLE);

    }
    public void showDatePickerDialog(View v) {
        DialogFragment newFragment = new DatePickerFragment();
        newFragment.show(getSupportFragmentManager(), "datePicker");
    }

    public static class DatePickerFragment extends DialogFragment
            implements DatePickerDialog.OnDateSetListener {

        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            // Use the current date as the default date in the picker
            final Calendar c = Calendar.getInstance();
            int year = c.get(Calendar.YEAR);
            int month = c.get(Calendar.MONTH);
            int day = c.get(Calendar.DAY_OF_MONTH);

            // Create a new instance of DatePickerDialog and return it
            return new DatePickerDialog(getActivity(), this, year, month, day);
        }

        public void onDateSet(DatePicker view, int year, int month, int day) {
            // Do something with the date chosen by the user
            Toast.makeText(getContext(), String.valueOf(year), Toast.LENGTH_SHORT).show();
            edittext_birthday.setText(String.valueOf(year)+"/"+String.valueOf(month)+"/"+String.valueOf(day));
        }
    }

}


