package com.company.trackify.Fragments;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.TextInputEditText;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.util.Log;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.company.trackify.CustomAdapters.CustomListAdapterForTeam;
import com.company.trackify.Members;
import com.company.trackify.R;
import com.company.trackify.ViewsHelper.NonScrollListView;
import com.company.trackify.helper.Constants;
import com.company.trackify.helper.DatabaseHelper;
import com.company.trackify.helper.MemberEntries;
import com.company.trackify.helper.MySingleton;
import com.company.trackify.helper.TeamEntries;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static android.content.Context.MODE_PRIVATE;


public class TeamFragment extends Fragment {
    JSONObject jsonObject;
    CustomListAdapterForTeam adapter;
    NonScrollListView listView;
    DatabaseHelper db;
    FloatingActionButton fab;
    String token = "";
    String userid = "";
    SharedPreferences shared;
    final String PREF_NAME = "USER";
    RelativeLayout layout;
    String teamid = "";

    public TeamFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    public void showTeams() {
        final List<TeamEntries> teamEntries = db.getTeamEntries();
        adapter = new CustomListAdapterForTeam(this.getActivity(), teamEntries);
        listView.setAdapter(adapter);
    }

    public void refresh() {

        final ProgressDialog pd = new ProgressDialog(getActivity());
        pd.setCancelable(false);
        pd.setCanceledOnTouchOutside(false);
        pd.setMessage("Refreshing.....");
        pd.show();
        StringRequest stringRequest = new StringRequest(Request.Method.POST, Constants.sync, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                pd.dismiss();
                try {
                    jsonObject = new JSONObject(response);
                    int resp = jsonObject.getInt("resp");
                    Log.d("teams", response);
                    if (resp == 0) {
                        String msg = jsonObject.getString("msg");
                        Toast.makeText(getActivity(), msg, Toast.LENGTH_SHORT).show();
                    } else {
                        DatabaseHelper db;
                        db = new DatabaseHelper(getActivity().getApplicationContext());
                        JSONArray teamArray = jsonObject.getJSONArray("team");
                        db.truncateTeam();
                        for (int i = 0; i < teamArray.length(); i++) {
                            String id = teamArray.getJSONObject(i).getString("id");
                            String user_id = teamArray.getJSONObject(i).getString("user_id");
                            String team_name = teamArray.getJSONObject(i).getString("team_name");
                            String team_desc = teamArray.getJSONObject(i).getString("team_desc");
                            String createdAt = teamArray.getJSONObject(i).getString("created_dt");
                            String is_active = teamArray.getJSONObject(i).getString("is_active");
                            TeamEntries userEntriesHelper = new TeamEntries(id, user_id, team_name, team_desc, createdAt, is_active);
                            Long ID = db.createTeamEntry(userEntriesHelper);
                            Log.d("LOG", ID.toString() + "hello world");
                        }
                        db.truncateMember();
                        JSONArray memberArray = jsonObject.getJSONArray("member");
                        for (int i = 0; i < memberArray.length(); i++) {
                            String id = memberArray.getJSONObject(i).getString("id");
                            String team_id = memberArray.getJSONObject(i).getString("team_id");
                            String member_id = memberArray.getJSONObject(i).getString("member_id");
                            String name = memberArray.getJSONObject(i).getString("name");
                            String createdAT = memberArray.getJSONObject(i).getString("created_dt");
                            String is_active = memberArray.getJSONObject(i).getString("is_active");
                            MemberEntries memberEntries = new MemberEntries(id, team_id, member_id, name, createdAT, is_active);
                            Long ID = db.createMemberEntry(memberEntries);
                            Log.d("LOG", ID.toString() + "hello omar");
                            Log.d("LOG", name + "hello omar");

                        }

                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                final List<TeamEntries> teamEntries = db.getTeamEntries();
                adapter = new CustomListAdapterForTeam(getActivity(), teamEntries);
                listView.setAdapter(adapter);
            }
        },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        pd.dismiss();
                        if (error instanceof TimeoutError || error instanceof NoConnectionError) {
                            Toast.makeText(getActivity(), "Please check your internet connection!", Toast.LENGTH_SHORT).show();
                        }
                    }
                }
        ) {
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("user_id", userid);
                params.put("token", token);
                return params;
            }
        };
        MySingleton singleton = MySingleton.getInstance(getActivity().getApplicationContext());
        singleton.getRequestQueue().add(stringRequest);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.activity_teams, container, false);
        layout = (RelativeLayout)rootView.findViewById(R.id.first_team);
        layout.setVisibility(View.GONE);
        shared = this.getActivity().getSharedPreferences(PREF_NAME, MODE_PRIVATE);
        userid = shared.getString("userid", "0");
        token = shared.getString("token", "0");
        db = new DatabaseHelper(getActivity().getApplicationContext());
        final List<TeamEntries> teamEntries = db.getTeamEntries();
        adapter = new CustomListAdapterForTeam(this.getActivity(), teamEntries);
        if(adapter.isEmpty()){
            Log.e("teams","empty");
            layout.setVisibility(View.VISIBLE);
            Button button = (Button)layout.findViewById(R.id.create_team);
            button.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    addTeam();
                }
            });
        }
        listView = (NonScrollListView) rootView.findViewById(R.id.teams_list);
        listView.setAdapter(adapter);
        fab = (FloatingActionButton) rootView.findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               addTeam();
            }
        });
        showTeams();
        registerForContextMenu(listView);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                try {

                    TeamEntries teamEntries1 = (TeamEntries) adapterView.getItemAtPosition(i);
                    String id = teamEntries1.getId();
                    Log.e("user id", id);
                    Intent intent = new Intent(getActivity().getApplicationContext(), Members.class);
                    intent.putExtra("DEVICEID", id);
                    startActivity(intent);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
        //some code
        return rootView;
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        if (v.getId() == R.id.teams_list) {
            ListView lv = (ListView) v;
            AdapterView.AdapterContextMenuInfo acmi = (AdapterView.AdapterContextMenuInfo) menuInfo;
            TeamEntries obj = (TeamEntries) lv.getItemAtPosition(acmi.position);
            teamid = obj.getId();

            menu.add("Delete");
        }
    }
    @Override
    public boolean onContextItemSelected(MenuItem item) {
        if(item.getTitle()=="Delete"){
            deleteTeam();
        }else{
            return false;
        }
        return true;
    }

    public void deleteTeam(){
        final ProgressDialog pd = new ProgressDialog(getActivity());
        pd.setCancelable(false);
        pd.setCanceledOnTouchOutside(false);
        pd.setMessage("Deleting the team...");
        pd.show();
        StringRequest stringRequest = new StringRequest(Request.Method.POST, Constants.deleteTeam, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                refresh();
                pd.dismiss();
                try {
                    jsonObject=new JSONObject(response);
                    int resp=jsonObject.getInt("resp");
                    String msg=jsonObject.getString("msg");
                    if(resp ==  1){
                        refresh();
                        Toast.makeText(getActivity(), msg, Toast.LENGTH_SHORT).show();

                    }else{
                        Toast.makeText(getActivity(), msg, Toast.LENGTH_SHORT).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        pd.dismiss();
                        if (error instanceof TimeoutError || error instanceof NoConnectionError){
                            Toast.makeText(getActivity(), "Please check your internet connection!", Toast.LENGTH_SHORT).show();
                        }
                    }
                }
        ) {
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("user_id", userid);
                params.put("token", token);
                params.put("team_id", teamid);
                return params;
            }
        };
        MySingleton singleton=MySingleton.getInstance(getActivity().getApplicationContext());
        singleton.getRequestQueue().add(stringRequest);
    }
    public void addTeam(){
        final Dialog dialog =new Dialog(getActivity());
        dialog.setContentView(R.layout.dialog_add_team);
        dialog.setTitle("Add Team");
        dialog.show();
        Button button_add=(Button)dialog.findViewById(R.id.btn_add);

        button_add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                TextInputEditText nameOfTeam= (TextInputEditText)dialog.findViewById(R.id.name);
                TextInputEditText descOfTeam= (TextInputEditText)dialog.findViewById(R.id.desc);
                final String name=nameOfTeam.getText().toString();
                final String desc=descOfTeam.getText().toString();
                if (name.equals("")|desc.equals("")){
                    Toast.makeText(getActivity(), "Please fill in the fields!", Toast.LENGTH_SHORT).show();
                    return;
                }
                final ProgressDialog pd = new ProgressDialog(getActivity());
                pd.setCancelable(false);
                pd.setCanceledOnTouchOutside(false);
                pd.setMessage("Adding the user...");
                pd.show();
                StringRequest stringRequest = new StringRequest(Request.Method.POST, Constants.addTeam, new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        try {
                            jsonObject=new JSONObject(response);
                            int resp=jsonObject.getInt("resp");
                            String msg=jsonObject.getString("msg");
                            if(resp ==  1){
                                refresh();
                                pd.dismiss();
                                dialog.dismiss();
                                layout.setVisibility(View.GONE);

                            }else{
                                Toast.makeText(getActivity(), msg, Toast.LENGTH_SHORT).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                        new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {
                                pd.dismiss();
                                if (error instanceof TimeoutError || error instanceof NoConnectionError){
                                    Toast.makeText(getActivity(), "Please check your internet connection!", Toast.LENGTH_SHORT).show();
                                }
                            }
                        }
                ) {
                    protected Map<String, String> getParams() {
                        Map<String, String> params = new HashMap<>();
                        params.put("token", token);
                        params.put("user_id", userid);
                        params.put("team_name", name);
                        params.put("team_desc", desc);
                        return params;
                    }
                };
                RequestQueue requestQueue = Volley.newRequestQueue(getActivity());
                requestQueue.add(stringRequest);
            }
        });



        dialog.show();
    }
}
