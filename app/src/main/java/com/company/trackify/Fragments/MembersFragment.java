package com.company.trackify.Fragments;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.company.trackify.CustomAdapters.CustomListAdapterForMember;
import com.company.trackify.Members;
import com.company.trackify.R;
import com.company.trackify.TrackActivity;
import com.company.trackify.ViewsHelper.NonScrollListView;
import com.company.trackify.helper.Constants;
import com.company.trackify.helper.DatabaseHelper;
import com.company.trackify.helper.MemberEntries;
import com.company.trackify.helper.MySingleton;
import com.company.trackify.helper.TeamEntries;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static android.content.Context.MODE_PRIVATE;

public class MembersFragment extends Fragment {

    NonScrollListView simpleList;
    String userid = "";
    String token="";
    SharedPreferences shared;
    final String PREF_NAME = "USER";
    CustomListAdapterForMember adapter;
    DatabaseHelper db;
    RelativeLayout layout;
    public MembersFragment(){
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.activity_members, container, false);
        simpleList=(NonScrollListView)rootView.findViewById(R.id.members_list);
        shared = getActivity().getSharedPreferences(PREF_NAME, MODE_PRIVATE);
        userid = shared.getString("userid", "0");
        token = shared.getString("token", "0");
        layout=(RelativeLayout)rootView.findViewById(R.id.first_member);
        layout.setVisibility(View.GONE);
        db = new DatabaseHelper(this.getActivity().getApplicationContext());
        final List<MemberEntries> memberEntries = db.getAllMemberEntries();


        adapter = new CustomListAdapterForMember(this.getActivity(), memberEntries);
        if(adapter.isEmpty()){
            Log.e("Members","empty");
            layout.setVisibility(View.VISIBLE);
//            Button button = (Button)layout.findViewById(R.id.add_member);
//            button.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//                    addMember();
//                }
//            });
        }
        simpleList.setAdapter(adapter);
        rootView.findViewById(R.id.fab).setVisibility(View.GONE);
        simpleList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                try {
                    MemberEntries memberEntries1= (MemberEntries) adapterView.getItemAtPosition(i);
                    Intent intent = new Intent(getActivity().getApplicationContext(),TrackActivity.class);
                    intent.putExtra("MEMBERID", memberEntries1.getMember_id());
                    startActivity(intent);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

//        fab.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                addMember();
//            }
//        });

        return rootView;
    }
//    public void addMember(){
//        final Dialog dialog = new Dialog(getActivity());
//        dialog.setContentView(R.layout.dialog_add_team);
//        TextInputLayout textInputLayout = (TextInputLayout) dialog.findViewById(R.id.input_desc);
//        textInputLayout.setVisibility(View.GONE);
//        Button button_add=(Button)dialog.findViewById(R.id.btn_add);
//        final TextInputEditText invitecode=(TextInputEditText) dialog.findViewById(R.id.name);
//        invitecode.setHint("Enter your Invite Code");
//        button_add.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                final String invite=invitecode.getText().toString();
//                if (invite.equals(" ")){
//                    Toast.makeText(getActivity(), "Please enter the invite code!", Toast.LENGTH_SHORT).show();
//                    return;
//                }
//                final ProgressDialog pd = new ProgressDialog(getActivity());
//                pd.setCancelable(false);
//                pd.setCanceledOnTouchOutside(false);
//                pd.setMessage("Adding the team member...");
//                pd.show();
//                StringRequest stringRequest = new StringRequest(Request.Method.POST, Constants.addMember, new Response.Listener<String>() {
//                    @Override
//                    public void onResponse(String response) {
//                        refresh();
//                        pd.dismiss();
//                        dialog.dismiss();
//                        try {
//                            jsonObject=new JSONObject(response);
//                            int resp=jsonObject.getInt("resp");
//                            String msg=jsonObject.getString("msg");
//                            if(resp ==  1){
//                                Toast.makeText(getActivity(), msg, Toast.LENGTH_SHORT).show();
//                                layout.setVisibility(View.GONE);
//
//                            }else{
//                                Toast.makeText(getActivity(), msg, Toast.LENGTH_SHORT).show();
//                            }
//                        } catch (JSONException e) {
//                            e.printStackTrace();
//                        }
//                    }
//                },
//                        new Response.ErrorListener() {
//                            @Override
//                            public void onErrorResponse(VolleyError error) {
//                                pd.dismiss();
//                                if (error instanceof TimeoutError || error instanceof NoConnectionError){
//                                    Toast.makeText(getActivity(), "Please check your internet connection!", Toast.LENGTH_SHORT).show();
//                                }
//                            }
//                        }
//                ) {
//                    protected Map<String, String> getParams() {
//                        Map<String, String> params = new HashMap<>();
//                        params.put("user_id", userid);
//                        params.put("token", token);
//                        params.put("inviteCode", invite);
//                        params.put("team_id", teamid);
//                        return params;
//                    }
//                };
//                MySingleton singleton=MySingleton.getInstance(getActivity().getApplicationContext());
//                singleton.getRequestQueue().add(stringRequest);
//            }
//        });
//        dialog.show();
//    }

}
