package com.company.trackify;

import android.Manifest;
import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.Image;
import android.net.Uri;
import android.os.Environment;
import android.os.RecoverySystem;
import android.os.StrictMode;
import android.provider.MediaStore;
import android.provider.Settings;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.AppCompatAutoCompleteTextView;
import android.test.suitebuilder.TestMethod;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.animation.GlideAnimation;
import com.bumptech.glide.request.target.SimpleTarget;
import com.company.trackify.ViewsHelper.EdittextInput;
import com.company.trackify.helper.Constants;
import com.company.trackify.helper.MySingleton;

import org.json.JSONException;
import org.json.JSONObject;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.logging.Logger;

import static android.R.attr.bitmap;

public class ProfileActivity extends AppCompatActivity implements View.OnClickListener {
    ImageView photo, photo_upload;
    Bitmap bitmap;
    TextView location_settings;
    ImageButton backButton;
    Button setSettings;
    EdittextInput full_name, address, country;
    private int PICK_IMAGE_REQUEST = 1;
    String distance= "50";
    String time = "600";
    String token="";
    String userid="";
    String memberID="";
    SharedPreferences shared;
    ProgressDialog loading;
    final String PREF_NAME = "USER";
    JSONObject jsonObject;
    String user_name,user_address,user_country;

    private static final int EXTERNAL_STORAGE_PERMISSION_CONSTANT = 100;
    private static final int REQUEST_PERMISSION_SETTING = 101;
    private boolean sentToSettings = false;
    private SharedPreferences permissionStatus;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);

        this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        permissionStatus = getSharedPreferences("permissionStatus", MODE_PRIVATE);
        backButton = (ImageButton) findViewById(R.id.backbutton);
        backButton.setOnClickListener(this);
        shared = getSharedPreferences(PREF_NAME, MODE_PRIVATE);
        photo = (ImageView) findViewById(R.id.photo);
        photo.setOnClickListener(this);
        photo_upload = (ImageView) findViewById(R.id.circularImage);
        userid = shared.getString("userid", "0");
        memberID = shared.getString("userid", "0");
        Log.e("profile", userid);
        token = shared.getString("token", "0");
        full_name = (EdittextInput)findViewById(R.id.full_name);
        address = (EdittextInput)findViewById(R.id.address);
        country = (EdittextInput)findViewById(R.id.country);

        user_name = shared.getString("name","0");
        if (user_name.equals(null)){
            user_name = " ";
        }
        user_address = shared.getString("address","0");

        if (user_address.equals(null)){
            user_address = " ";
        }
        user_country = shared.getString("country","0");
        if (user_country.equals(null)){
            user_country = " ";
        }
        full_name.setText(user_name);
        address.setText(user_address);
        country.setText(user_country);


        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);
        setSettings = (Button)findViewById(R.id.btn_set) ;
        setSettings.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setSettings();
            }
        });
        location_settings = (TextView)findViewById(R.id.item3);
        location_settings.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showDialog();
            }
        });
//        File imgFile = new  File("/storage/emulated/0/Trackify/ProfileImage.png");
//
//        if(imgFile.exists()){
//
//            Bitmap myBitmap = BitmapFactory.decodeFile("/storage/emulated/0/Trackify/ProfileImage.png");
//
//            photo_upload.setImageBitmap(myBitmap);
//
//        }
        Glide.with(this).
                load("https://agro-mycollege.c9users.io/profileImage/" + memberID + ".png")
                .asBitmap()
                .fitCenter()
                .dontTransform()
                .diskCacheStrategy(DiskCacheStrategy.NONE)
                .skipMemoryCache(true)
                .into(new SimpleTarget<Bitmap>() {
                    @Override
                    public void onResourceReady(Bitmap bitmap, GlideAnimation<? super Bitmap> glideAnimation) {
                        Log.e("map", "we are in profile activity now!" + memberID);

                        photo_upload.setImageBitmap(bitmap);
                    }
                });
    }
    public void setSettings(){
        Log.e("jello",userid + token + " " + distance + " " + time);
        user_name = full_name.getText().toString();
        user_address = address.getText().toString();
        user_country = country.getText().toString();
        final ProgressDialog pd = new ProgressDialog(ProfileActivity.this);
        pd.setCancelable(false);
        pd.setCanceledOnTouchOutside(false);
        pd.setMessage("Loading...");
        pd.show();
        StringRequest stringRequest = new StringRequest(Request.Method.POST, Constants.setProfile, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                pd.dismiss();
                try {

                    jsonObject=new JSONObject(response);
                    int resp=jsonObject.getInt("resp");
                    if(resp ==  1){
                        SharedPreferences sharedpreferences = getSharedPreferences("USER", Context.MODE_PRIVATE);
                        SharedPreferences.Editor editor = sharedpreferences.edit();
                        editor.putString("name",user_name);
                        editor.putString("address",user_address);
                        editor.putString("country",user_country);
                        editor.apply();
                        Toast.makeText(ProfileActivity.this, "Settings successfully saved!", Toast.LENGTH_LONG).show();
                        Intent intent = new Intent(getApplicationContext(),ProfileFinal.class);
                        startActivity(intent);
                        finish();

                    }else{

                        String msg = jsonObject.getString("msg");
                        Toast.makeText(ProfileActivity.this, msg, Toast.LENGTH_SHORT).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                        pd.dismiss();
                        if (error instanceof TimeoutError || error instanceof NoConnectionError){
                            Toast.makeText(ProfileActivity.this, "Please check your internet connection!", Toast.LENGTH_SHORT).show();
                        }
                    }
                }
        ) {
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("user_id", userid);
                params.put("token", token);
                params.put("name", user_name );
                params.put("address", user_address);
                params.put("country", user_country);
                params.put("location_accuracy_distance", distance);
                params.put("location_accuracy_time", time);
                return params;
            }
        };
        MySingleton singleton=MySingleton.getInstance(getApplicationContext());
        singleton.getRequestQueue().add(stringRequest);
    }
    public void showDialog(){
        final Dialog dialog =new Dialog(ProfileActivity.this);
        dialog.setContentView(R.layout.dialog_settings);
        dialog.setTitle("LOCATION SETTINGS");
        dialog.show();
        final Spinner forDistance=(Spinner)dialog.findViewById(R.id.accuracy_distance);
        final Spinner forTime=(Spinner)dialog.findViewById(R.id.accuracy_time);
        ArrayAdapter<CharSequence> adapterForDistance = ArrayAdapter.createFromResource(this,
                R.array.location_distance, android.R.layout.simple_spinner_item);
        adapterForDistance.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        forDistance.setAdapter(adapterForDistance);

        ArrayAdapter<CharSequence> adapterForTime = ArrayAdapter.createFromResource(this,
                R.array.location_time, android.R.layout.simple_spinner_item);
        adapterForTime.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        forTime.setAdapter(adapterForTime);
        Button btn_settings = (Button)dialog.findViewById(R.id.btn_settings);
        btn_settings.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                distance = forDistance.getSelectedItem().toString();
                String time1 = forTime.getSelectedItem().toString();
                int time2 = Integer.parseInt(time1)*60;
                time=String.valueOf(time2);
                dialog.dismiss();
            }
        });
    }

    public void checkPermission(){
        if (ActivityCompat.checkSelfPermission(ProfileActivity.this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(ProfileActivity.this, Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
                //Show Information about why you need the permission
                AlertDialog.Builder builder = new AlertDialog.Builder(ProfileActivity.this);
                builder.setTitle("Need Storage Permission");
                builder.setMessage("This app needs storage permission.");
                builder.setPositiveButton("Grant", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                        ActivityCompat.requestPermissions(ProfileActivity.this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, EXTERNAL_STORAGE_PERMISSION_CONSTANT);
                    }
                });
                builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });
                builder.show();
            } else if (permissionStatus.getBoolean(Manifest.permission.WRITE_EXTERNAL_STORAGE,false)) {
                //Previously Permission Request was cancelled with 'Dont Ask Again',
                // Redirect to Settings after showing Information about why you need the permission
                AlertDialog.Builder builder = new AlertDialog.Builder(ProfileActivity.this);
                builder.setTitle("Need Storage Permission");
                builder.setMessage("This app needs storage permission.");
                builder.setPositiveButton("Grant", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                        sentToSettings = true;
                        Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                        Uri uri = Uri.fromParts("package", getPackageName(), null);
                        intent.setData(uri);
                        startActivityForResult(intent, REQUEST_PERMISSION_SETTING);
                        Toast.makeText(getBaseContext(), "Go to Permissions to Grant Storage", Toast.LENGTH_LONG).show();
                    }
                });
                builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });
                builder.show();
            } else {
                //just request the permission
                ActivityCompat.requestPermissions(ProfileActivity.this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, EXTERNAL_STORAGE_PERMISSION_CONSTANT);
            }


            SharedPreferences.Editor editor = permissionStatus.edit();
            editor.putBoolean(Manifest.permission.WRITE_EXTERNAL_STORAGE,true);
            editor.commit();




        } else {
            //You already have the permission, just go ahead.
            showFileChooser();
        }
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == EXTERNAL_STORAGE_PERMISSION_CONSTANT) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                //The External Storage Write Permission is granted to you... Continue your left job...
                showFileChooser();
            } else {
                if (ActivityCompat.shouldShowRequestPermissionRationale(ProfileActivity.this, Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
                    //Show Information about why you need the permission
                    AlertDialog.Builder builder = new AlertDialog.Builder(ProfileActivity.this);
                    builder.setTitle("Need Storage Permission");
                    builder.setMessage("This app needs storage permission");
                    builder.setPositiveButton("Grant", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.cancel();


                            ActivityCompat.requestPermissions(ProfileActivity.this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, EXTERNAL_STORAGE_PERMISSION_CONSTANT);


                        }
                    });
                    builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.cancel();
                        }
                    });
                    builder.show();
                } else {
                    Toast.makeText(getBaseContext(),"Unable to get Permission",Toast.LENGTH_LONG).show();
                }
            }
        }
    }
//    public void setProfile(){
//        final String Name=name.getText().toString();
//        final String Address=address.getText().toString();
//        final String Country=country.getText().toString();
//        if (Name.equals("")){
//            Toast.makeText(this, "Please enter your name!", Toast.LENGTH_SHORT).show();
//            return;
//        }else if (Address.equals("")){
//            Toast.makeText(this, "Please enter your address!", Toast.LENGTH_SHORT).show();
//            return;
//        }else if (Country.equals("")){
//            Toast.makeText(this, "Please enter your country", Toast.LENGTH_SHORT).show();
//        }
//        final ProgressDialog pd = new ProgressDialog(ProfileActivity.this);
//        pd.setCancelable(false);
//        pd.setCanceledOnTouchOutside(false);
//        pd.setMessage("Updating...");
//        pd.show();
//
//        StringRequest stringRequest = new StringRequest(Request.Method.POST, Constants.setProfile, new Response.Listener<String>() {
//            @Override
//            public void onResponse(String response) {
//
//                pd.dismiss();
//                try {
//
//                    jsonObject=new JSONObject(response);
//                    int resp=jsonObject.getInt("resp");
//
//                    String msg = jsonObject.getString("msg");
//                    if(resp ==  1){
//                        Toast.makeText(ProfileActivity.this, msg, Toast.LENGTH_SHORT).show();
//                        startActivity(new Intent(ProfileActivity.this,HomeActivity.class));
//                        finish();
//
//                    }else{
//
//                        Toast.makeText(ProfileActivity.this, msg, Toast.LENGTH_SHORT).show();
//
//                    }
//                } catch (JSONException e) {
//                    e.printStackTrace();
//                }
//            }
//        },
//                new Response.ErrorListener() {
//                    @Override
//                    public void onErrorResponse(VolleyError error) {
//                        pd.dismiss();
//                        if (error instanceof TimeoutError || error instanceof NoConnectionError){
//                            Toast.makeText(ProfileActivity.this, "Please check your internet connection!", Toast.LENGTH_SHORT).show();
//                        }
//                    }
//                }
//        ) {
//            protected Map<String, String> getParams() {
//                Map<String, String> params = new HashMap<>();
//                params.put("user_id", userid);
//                params.put("token", token);
//                params.put("name", Name);
//                params.put("address", Address);
//                params.put("country", Country);
//                return params;
//            }
//        };
//        MySingleton singleton=MySingleton.getInstance(getApplicationContext());
//        singleton.getRequestQueue().add(stringRequest);
//    }

    public void setProfilePhoto(){
        loading = ProgressDialog.show(this,"Uploading...","Please wait...",true,false);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, Constants.setProfilePhoto,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String s) {
                        //Disimissing the progress dialog
                        loading.dismiss();
                        //Showing toast message of the response
                        Toast.makeText(ProfileActivity.this, s , Toast.LENGTH_LONG).show();
                        try {
                            jsonObject=new JSONObject(s);
                            int resp=jsonObject.getInt("resp");
                            String msg = jsonObject.getString("msg");
                            if (resp == 1){
                                Log.e("url",msg);
//                                svgPhoto(msg);

                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError volleyError) {
                        //Dismissing the progress dialog
                        loading.dismiss();
                        //Showing toast
                        Toast.makeText(ProfileActivity.this, volleyError.toString(), Toast.LENGTH_LONG).show();
                    }
                }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                //Converting Bitmap to String
                String image = getStringImage(bitmap);

                Map<String,String> params = new Hashtable<String, String>();
                params.put("user_id",userid);
                params.put("token",token);
                params.put("profileImage",image);

                Log.e("aldkf",userid + "  "+ token + "  "+image);
                //returning parameters
                return params;
            }
        };

        //Creating a Request Queue
        RequestQueue requestQueue = Volley.newRequestQueue(this);

        //Adding request to the queue
        requestQueue.add(stringRequest);
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                15 * 1000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
    }

    private void showFileChooser() {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(intent, "Select Picture"), PICK_IMAGE_REQUEST);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == PICK_IMAGE_REQUEST && resultCode == RESULT_OK && data != null && data.getData() != null) {
            Uri filePath = data.getData();
            try {

                //Getting the Bitmap from Gallery
                bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), filePath);
                //Setting the Bitmap to ImageView

                photo_upload.setImageBitmap(bitmap);
                setProfilePhoto();

            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        if (requestCode == REQUEST_PERMISSION_SETTING) {
            if (ActivityCompat.checkSelfPermission(ProfileActivity.this, android.Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
                //Got Permission
                showFileChooser();
            }
        }
    }
    @Override
    protected void onPostResume() {
        super.onPostResume();
        if (sentToSettings) {
            if (ActivityCompat.checkSelfPermission(ProfileActivity.this, android.Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
                //Got Permission
                showFileChooser();
            }
        }
    }
    public String getStringImage(Bitmap bmp){
        ByteArrayOutputStream byteArrayOS = new ByteArrayOutputStream();
        bmp.compress(Bitmap.CompressFormat.PNG, 100, byteArrayOS);
        return Base64.encodeToString(byteArrayOS.toByteArray(), Base64.DEFAULT);
    }
    public void svgPhoto(String url) throws IOException {
        File f = new File(Environment.getExternalStorageDirectory() + "/Trackify");
        if (f.exists()){
            Log.e("tag","hurray");
            URL url1 = new URL (url);
            InputStream input = url1.openStream();
            try {
                //The sdcard directory e.g. '/sdcard' can be used directly, or
                //more safely abstracted with getExternalStorageDirectory()
                OutputStream output = new FileOutputStream (new File("/storage/emulated/0/Trackify/","ProfileImage.png"));
                try {
                    byte[] buffer = new byte[1028];
                    int bytesRead = 0;
                    while ((bytesRead = input.read(buffer, 0, buffer.length)) >= 0) {
                        output.write(buffer, 0, bytesRead);
                    }
                } finally {
                    output.close();
                }
            } finally {
                input.close();
            }
        }else {
            Log.e("tag","not found");
            File dir = new File(Environment.getExternalStorageDirectory() + "/Trackify");
            boolean b = dir.mkdir();
            if (b){
                URL url1 = new URL (url);
                InputStream input = url1.openStream();
                try {
                    //The sdcard directory e.g. '/sdcard' can be used directly, or
                    //more safely abstracted with getExternalStorageDirectory()
                    OutputStream output = new FileOutputStream (new File("/storage/emulated/0/Trackify/","ProfileImage.png"));
                    try {
                        byte[] buffer = new byte[1028];
                        int bytesRead = 0;
                        while ((bytesRead = input.read(buffer, 0, buffer.length)) >= 0) {
                            output.write(buffer, 0, bytesRead);
                        }
                    } finally {
                        output.close();
                    }
                } finally {
                    input.close();
                }
            }

        }








//        URL url1 = new URL (url);
//        InputStream input = url1.openStream();
//        try {
//            //The sdcard directory e.g. '/sdcard' can be used directly, or
//            //more safely abstracted with getExternalStorageDirectory()
//            File storagePath = Environment.getExternalStorageDirectory();
//            OutputStream output = new FileOutputStream (new File(storagePath,"myImage.png"));
//
//            Log.e("profile",storagePath.toString());
//
//            try {
//                byte[] buffer = new byte[200];
//                int bytesRead = 0;
//                while ((bytesRead = input.read(buffer, 0, buffer.length)) >= 0) {
//                    output.write(buffer, 0, bytesRead);
//                }
//            } finally {
//                output.close();
//            }
//        } finally {
//            input.close();
//        }
    }


    @Override
    public void onClick(View v) {
//        if(v==btn_save){
//            setProfile();
        if (v == photo){
            checkPermission();
        }
        if (v == backButton){
            onBackPressed();
        }
    }
}
