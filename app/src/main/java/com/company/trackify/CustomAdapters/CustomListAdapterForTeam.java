package com.company.trackify.CustomAdapters;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.amulyakhare.textdrawable.TextDrawable;
import com.amulyakhare.textdrawable.util.ColorGenerator;
import com.company.trackify.R;
import com.company.trackify.helper.TeamEntries;

import java.util.List;

/**
 * Created by Omar on 24-03-2017.
 */

public class CustomListAdapterForTeam extends BaseAdapter {
    private Activity activity;
    private List<TeamEntries> teamEntries;
    private LayoutInflater inflater;

    public CustomListAdapterForTeam(){
    }

    public CustomListAdapterForTeam(Activity activity, List<TeamEntries> teamEntriesList){
        this.activity = activity;
        this.teamEntries = teamEntriesList;

    }
    @Override
    public int getCount() {
        return teamEntries.size();
    }

    @Override
    public Object getItem(int position) {
        return teamEntries.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        if(inflater == null){
            inflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }

        if(convertView == null){
            convertView = inflater.inflate(R.layout.activity_teams_list,null);
        }

        TextView team_name = (TextView) convertView.findViewById(R.id.team_name);
        TextView team_desc = (TextView) convertView.findViewById(R.id.team_desc);
        ImageView team_image = (ImageView) convertView.findViewById(R.id.list_image);
        TeamEntries i = teamEntries.get(position);
        team_name.setText(i.getTeam_name());
        team_desc.setText(i.getTeam_desc());

        String firstLetter = String.valueOf(i.getTeam_name().charAt(0));
        ColorGenerator generator = ColorGenerator.MATERIAL;
        int color = generator.getColor(getItem(position));
        //int color = generator.getRandomColor();

        TextDrawable drawable = TextDrawable.builder()
                .buildRound(firstLetter, color); // radius in px

        team_image.setImageDrawable(drawable);

        return convertView;
    }
}
