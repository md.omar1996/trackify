package com.company.trackify.CustomAdapters;

import android.app.Activity;
import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.company.trackify.R;
import com.company.trackify.helper.MemberEntries;
import com.company.trackify.helper.TeamEntries;

import java.util.List;

/**
 * Created by Omar on 24-03-2017.
 */

public class CustomListAdapterForMember extends BaseAdapter {
    private Activity activity;
    private List<MemberEntries> memberEntries;
    private LayoutInflater inflater;

    public CustomListAdapterForMember(){
    }

    public CustomListAdapterForMember(Activity activity, List<MemberEntries> memberEntriesList){
        this.activity = activity;
        this.memberEntries = memberEntriesList;

    }
    @Override
    public int getCount() {
        return memberEntries.size();
    }

    @Override
    public Object getItem(int position) {
        return memberEntries.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        if(inflater == null){
            inflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }

        if(convertView == null){
            convertView = inflater.inflate(R.layout.activity_members_list,null);
        }

        TextView member_name = (TextView) convertView.findViewById(R.id.team_name);
        MemberEntries i = memberEntries.get(position);
        member_name.setText(i.getName());

        return convertView;
    }
}
